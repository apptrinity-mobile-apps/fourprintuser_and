package com.indobytes.a4printuser

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.Helper.ConnectionManager
import com.indobytes.a4printuser.model.APIResponse.UpdateProfileResponse
import com.indobytes.a4printuser.model.ApiInterface
import kotlinx.android.synthetic.main.app_bar_main.*
import retrofit2.Call
import retrofit2.Callback

class ProfileEditActivity : AppCompatActivity() {

    internal lateinit var et_first_id: EditText
    internal lateinit var et_last_id:EditText
    internal lateinit var et_user_name_id:EditText
    internal lateinit var et_email_id:EditText
    internal lateinit var et_company_id:EditText
    internal lateinit var et_phone_no_id:EditText
    internal lateinit var et_alter_no_id:EditText
    internal lateinit var tv_update_profile_id: TextView
    internal lateinit var et_cust_ser_id: TextView
    internal lateinit var et_ref_id:TextView
    internal lateinit var cust_serv_selected_stg: String
    internal lateinit var refer_select_stg:String
    var list_items_list: ListView? = null
    var dialog_list: Dialog?=null
    val csrArr = listOf("No Preference", "Keith MacKenZie", "Rene Sanchez", "Nate Bartlett", "Eric Cameron", "Brian Sosin", "Cori Corbino", "Luis Rosales")
    val referedByArr = listOf("Internet Search", "Magazine", "Vehicle Ad", "Flyer", "Billboard Ad", "Interner Ad", "Printing Tag", "Social Network", "Direct Mail Postcard", "Radio Commercial", "Client Referral", "Sales Rep", "Below Zero Graphics", "TV Comercial")
    internal lateinit var first_name_stg_get: String
    internal lateinit var last_name_stg_get:String
    internal lateinit var company_stg_get:String
    internal lateinit var phone_stg_get:String
    internal lateinit var alter_phone_stg_get:String
    internal lateinit var email_stg_get:String
    internal lateinit var username:String
    internal lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_edit)
        sessionManager = SessionManager(this)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setTitle("PROFILE EDIT")
        var rootview = findViewById(R.id.rootview) as LinearLayout
        myloading()
        et_first_id = findViewById(R.id.et_first_id) as EditText
        et_last_id = findViewById(R.id.et_last_id) as EditText
        et_user_name_id = findViewById(R.id.et_user_name_id) as EditText
        et_email_id = findViewById(R.id.et_email_id) as EditText
        et_company_id = findViewById(R.id.et_company_id) as EditText
        et_phone_no_id = findViewById(R.id.et_phone_no_id) as EditText
        et_alter_no_id = findViewById(R.id.et_alter_no_id) as EditText

        et_cust_ser_id = findViewById(R.id.et_cust_serv_id) as TextView
        et_ref_id = findViewById(R.id.et_ref_id) as TextView

        tv_update_profile_id = findViewById(R.id.tv_update_profile_id) as TextView

        if(intent != null && intent.getStringExtra("username")!=null && intent.getStringExtra("email")!=null){
            username = intent.getStringExtra("username")
            email_stg_get = intent.getStringExtra("email")
            first_name_stg_get = intent.getStringExtra("firstname")
            last_name_stg_get = intent.getStringExtra("lastname")
            company_stg_get = intent.getStringExtra("company")
            phone_stg_get = intent.getStringExtra("phonenumber")
            alter_phone_stg_get = intent.getStringExtra("alternatenumber")
            cust_serv_selected_stg = intent.getStringExtra("csr")
            refer_select_stg = intent.getStringExtra("refferrby")

            et_first_id.setText(first_name_stg_get)
            et_last_id.setText(last_name_stg_get)
            et_user_name_id.setText(username)
            et_email_id.setText(email_stg_get)
            et_company_id.setText(company_stg_get)
            et_phone_no_id.setText(phone_stg_get)
            et_alter_no_id.setText(alter_phone_stg_get)
            // et_cust_serv_id.setTooltipText(cust_serv_stg);
            et_cust_ser_id.setText(cust_serv_selected_stg)
            et_ref_id.setText(refer_select_stg)

        }





        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)

        list_items_list = dialog_list!!.findViewById(R.id.list_languages)


        tv_update_profile_id.setOnClickListener(View.OnClickListener {
            first_name_stg_get = et_first_id.getText().toString().trim({ it <= ' ' })
            last_name_stg_get = et_last_id.getText().toString().trim({ it <= ' ' })
            company_stg_get = et_company_id.getText().toString().trim({ it <= ' ' })
            phone_stg_get = et_phone_no_id.getText().toString().trim({ it <= ' ' })
            alter_phone_stg_get = et_alter_no_id.getText().toString().trim({ it <= ' ' })


            if (hasValidCredentials()) {
                if(ConnectionManager.checkConnection(applicationContext)) {
                    CallUpdateProfile(sessionManager.isUserId,et_first_id.getText().toString(),et_last_id.getText().toString(),et_company_id.getText().toString(), et_phone_no_id.getText().toString(),et_alter_no_id.getText().toString(),cust_serv_selected_stg,refer_select_stg)
                }
                else{
                    ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview,applicationContext)
                }

            }
        })


        et_ref_id.setOnClickListener(View.OnClickListener {
            if (!dialog_list!!.isShowing())
                dialog_list!!.show()
            val state_array_adapter = ArrayAdapter<String>(this@ProfileEditActivity, android.R.layout.simple_spinner_item, referedByArr)
            list_items_list!!.setAdapter(state_array_adapter)
            list_items_list!!.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                refer_select_stg = list_items_list!!.getItemAtPosition(i).toString()
                et_ref_id.setText(refer_select_stg)
                dialog_list!!.dismiss()
            })
        })
        et_cust_ser_id.setOnClickListener(View.OnClickListener {
            if (!dialog_list!!.isShowing())
                dialog_list!!.show()
            val state_array_adapter = ArrayAdapter<String>(this@ProfileEditActivity, android.R.layout.simple_spinner_item, csrArr)
            list_items_list!!.setAdapter(state_array_adapter)
            list_items_list!!.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                cust_serv_selected_stg = list_items_list!!.getItemAtPosition(i).toString()
                et_cust_ser_id.setText(cust_serv_selected_stg)
                dialog_list!!.dismiss()
            })
        })
    }

    private fun CallUpdateProfile(user_id: String, firstname: String, lastname: String, company: String, phonenumber1: String, phonenumber2: String, cust_serv_selected_stg: String, refer_select_stg: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.updateProfile(ApiInterface.ENCRIPTION_KEY,user_id,firstname,lastname,company,phonenumber1,phonenumber2,cust_serv_selected_stg,refer_select_stg)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UpdateProfileResponse> {
            override fun onResponse(call: Call<UpdateProfileResponse>, response: retrofit2.Response<UpdateProfileResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1")) {
                        val intent = Intent(this@ProfileEditActivity,ProfileInfoActivity::class.java)
                        finish()
                        startActivity(intent)
                    }

                }
            }

            override fun onFailure(call: Call<UpdateProfileResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(first_name_stg_get)) {
            et_first_id.setError("Please Provide First Name")
        } else if (TextUtils.isEmpty(last_name_stg_get)) {
            et_first_id.setError("Please Provide Last Name")
        } else if (TextUtils.isEmpty(company_stg_get)) {
            et_company_id.setError("Please Provide Company Name")
        } else if (TextUtils.isEmpty(phone_stg_get)) {
            et_phone_no_id.setError("Please Provide Phone Number")
        } else
            return true
        return false
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
}