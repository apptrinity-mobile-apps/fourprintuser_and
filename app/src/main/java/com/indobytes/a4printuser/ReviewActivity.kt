package com.indobytes.a4printuser

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.Helper.*
import com.indobytes.a4printuser.model.APIResponse.Coupon
import com.indobytes.a4printuser.model.APIResponse.Product
import com.indobytes.a4printuser.model.APIResponse.UPSObject
import com.indobytes.a4printuser.model.APIResponse.UPSObjectwithCoupon
import com.indobytes.a4printuser.model.ApiInterface
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.RequestParams
import kotlinx.android.synthetic.main.app_bar_main.*
import org.json.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.lang.ref.WeakReference
import java.math.RoundingMode
import java.net.HttpURLConnection
import java.net.URL
import java.util.*


class ReviewActivity : AppCompatActivity() , View.OnClickListener  {




    private lateinit var toolbar: Toolbar

    internal lateinit var ll_order:LinearLayout
    internal lateinit var ll_upload:LinearLayout
    internal lateinit var ll_shipping:LinearLayout
    internal lateinit var ll_review:LinearLayout

    internal lateinit var ll_ordersummary:LinearLayout
    internal lateinit var ll_billing:LinearLayout
    internal lateinit var ll_shipping_order:LinearLayout
    internal lateinit var ll_pricing:LinearLayout

    internal lateinit var ll_ordersummary_info:LinearLayout
    internal lateinit var ll_billing_info:LinearLayout
    internal lateinit var ll_shipping_order_info:LinearLayout
    internal lateinit var ll_pricing_info:LinearLayout

    internal lateinit var tv_ordersummary_edit:TextView
    internal lateinit var tv_billing_edit:TextView
    internal lateinit var tv_shipping_edit:TextView
    internal lateinit var tv_pricing_edit:TextView


    internal lateinit var et_summaryproduct_name:EditText
    internal lateinit var et_summaryquantity:EditText
    internal lateinit var et_summaryexactquantity:EditText
    internal lateinit var et_summaryprintsize:EditText
    internal lateinit var et_summaryexactsize:EditText
    internal lateinit var et_summarysides:EditText
    internal lateinit var et_summarypaperstock:EditText
    internal lateinit var et_summarycoating:EditText
    internal lateinit var et_summarypages:EditText
    internal lateinit var et_summarycolorcritical:EditText
    internal lateinit var et_summarycoverpapertype:EditText
    internal lateinit var et_summarybinding:EditText
    internal lateinit var et_summarypricetype:EditText


     var st_summaryproduct_name:String=""
     var st_summaryquantity:String=""
     var st_summaryexactquantity:String=""
     var st_summaryprintsize:String=""
     var st_summaryexactsize:String=""
     var st_summarysides:String=""
     var st_summarypaperstock:String=""
     var st_summarycoating:String=""
     var st_summarypages:String=""
     var st_summarycolorcritical:String=""
     var st_summarycoverpapertype:String=""
     var st_summarybinding:String=""
     var st_summarypricetype:String=""
     var st_paper_type:String=""

     var st_custom_design:String=""
     var st_design_instructions:String=""
     var st_sdesign_notes:String=""
     var st_design_back_text:String=""
     var st_design_front_text:String=""

    var st_crazy_fast_price:String=""
    var st_economy_price:String=""
    var st_fast_price:String=""
    var st_offercode:String=""
    var st_faster_price:String=""



    internal lateinit var tv_billing_name:EditText
    internal lateinit var tv_billing_address:EditText
    internal lateinit var tv_billing_address2:EditText
    internal lateinit var tv_billing_city:EditText
    internal lateinit var tv_billing_state: CustomTextView
    internal lateinit var tv_billing_zipcode:EditText

    internal lateinit var tv_shipping_method:EditText
    internal lateinit var tv_shipping_name:EditText
    internal lateinit var tv_shipping_company:EditText
    internal lateinit var tv_shipping_address:EditText
    internal lateinit var tv_shipping_addresstype:CustomTextView
    internal lateinit var tv_shipping_city:EditText
    internal lateinit var tv_shipping_state: CustomTextView
    internal lateinit var tv_shipping_zipcode:EditText

    internal lateinit var et_pricing_printing:TextView
    internal lateinit var et_pricing_shipping:TextView
    internal lateinit var et_totalprice:TextView


    internal lateinit var tv_billing_btn_submit:Button
    internal lateinit var tv_shipping_btn_submit:Button
    internal lateinit var btn_previous_screen:Button
    internal lateinit var btn_submit_order:Button
    internal lateinit var btn_save_order:Button
    internal lateinit var rootview:RelativeLayout


    var dialog_list: Dialog?=null
    var list_items_list: ListView? = null
    val addresstype = listOf("Commercial", "Residental")
    internal lateinit var sessionManager: SessionManager

    var billing_info_name:String=""
    var billing_address:String=""
     var billing_info_address2:String=""
     var billing_info_city:String=""
     var billing_info_state:String=""
     var billing_info_zipcode:String=""
     var billing_info_select_profile:String=""
     var billing_info_new_profile:String="0"
     var billing_info_default_profile:String="0"


     var shipping_address_name:String=""
     var shiping_address:String=""
     var shiping_address_type:String=""
     var shipping_address_company:String=""
     var shipping_address_address2:String=""
     var shipping_address_city:String=""
     var shipping_address_state:String=""
     var shipping_address_zipcode:String=""
     var shipping_address_select_profile:String=""
     var shipping_address_new_profile:String="0"
     var shipping_address_default_profile:String="0"


     var shipping_info_email:String=""
     var shiping_cc_refer:String=""
     var shipping_blind_shipping_check:String="0"

     var shipping_method_name:String=""
     var shipping_price:String="0.00"
     var shipping_delivery:String=""
     var pricing_value:String="0.00"
     var productid:String=""
     var pricingid:String=""
     var Price_type:String=""
    var ups_zipcode: String = ""
    var upsprice: String = ""


    var upload_notes: String = ""
    var upload_instruction: String = ""
    var upload_front: String = ""
    var upload_back: String = ""

    var mUPSProductList: ArrayList<Product> = ArrayList<Product>()
    var mUPScouponList: ArrayList<Coupon> = ArrayList<Coupon>()
    internal lateinit var upsobj:UPSObject
    internal lateinit var upsobj2:UPSObjectwithCoupon

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setTitle("REVIEW ORDER")
        rootview = findViewById(R.id.rootview) as RelativeLayout
        sessionManager = SessionManager(this)
        readBundleData()
        initialization()
        dialogInitialization()
        try {
            setBundleToView()
        }catch (e:Exception){e.printStackTrace()}

        Log.w("States","States"+sessionManager.states)
        myloading()
    }

    private fun setBundleToView() {
        tv_billing_name.setText(billing_info_name)
        tv_billing_address.setText(billing_address)
        tv_billing_address2.setText(billing_info_address2)
        tv_billing_city.setText(billing_info_city)
        tv_billing_state.setText(billing_info_state)
        tv_billing_zipcode.setText(billing_info_zipcode)


        //shipping components
        tv_shipping_method.setText(shipping_address_name)
        tv_shipping_name.setText(shipping_address_name)
        tv_shipping_company.setText(shipping_address_company)
        tv_shipping_address.setText(shiping_address)
        tv_shipping_addresstype.setText(shiping_address_type)
        tv_shipping_state.setText(shipping_address_state)
        tv_shipping_city.setText(shipping_address_city)
        tv_shipping_zipcode.setText(shipping_address_zipcode)

        mUPSProductList.clear()
        mUPScouponList.clear()
        val gson = Gson()
        val json = sessionManager.getobject
        val json2 = json.toString()
        if (json2 != null) {
            if (json2.contains("coupon") && json2.contains("offercode")) {
                upsobj2 = gson.fromJson(json2, UPSObjectwithCoupon::class.java)
                if (upsobj2.product != null) {
                    mUPSProductList.add(upsobj2.product!!)
                }
                if (upsobj2.coupon != null) {
                    mUPScouponList.add(upsobj2.coupon!!)
                }
                if (upsobj2.zipcode != null)
                    ups_zipcode = upsobj2.zipcode!!
                if (upsobj2.upsprice != null)
                    upsprice = upsobj2.upsprice!!
            } else {
                upsobj = gson.fromJson(json2, UPSObject::class.java)
                if (upsobj.product != null) {
                    mUPSProductList.add(upsobj.product!!)
                }
                if (upsobj.zipcode != null)
                    ups_zipcode = upsobj.zipcode!!
                if (upsobj.upsprice != null)
                    upsprice = upsobj.upsprice!!
            }


        }

        if (mUPSProductList.size > 0) {
            if (mUPSProductList.get(0).Price != null)
                pricing_value = mUPSProductList.get(0).Price!!
            if (mUPSProductList.get(0).Price_type != null)
                Price_type = mUPSProductList.get(0).Price_type!!
            if (mUPSProductList.get(0).pricingid != null)
                pricingid = mUPSProductList.get(0).pricingid!!
            if (mUPSProductList.get(0).productid != null)
                productid = mUPSProductList.get(0).productid!!

            //ordersummary inforamtion from saved data
            /*-----------------------------------------*/
            if (mUPSProductList.get(0).productname != null) {
                st_summaryproduct_name = mUPSProductList.get(0).productname!!
            }
            if (mUPSProductList.get(0).paper_type != null ) {
                st_paper_type = mUPSProductList.get(0).paper_type!!
            }
            if (mUPSProductList.get(0).product_meta!!.Quantity != null)
                st_summaryquantity = mUPSProductList.get(0).product_meta!!.Quantity!!
            if (mUPSProductList.get(0).product_meta!!.Exact_Quantity != null)
                st_summaryexactquantity = mUPSProductList.get(0).product_meta!!.Exact_Quantity!!
            if (mUPSProductList.get(0).product_meta!!.PRINT_SIZE != null)
                st_summaryprintsize = mUPSProductList.get(0).product_meta!!.PRINT_SIZE!!
            if (mUPSProductList.get(0).product_meta!!.EXACT_SIZE != null)
                st_summaryexactsize = mUPSProductList.get(0).product_meta!!.EXACT_SIZE!!
            if (mUPSProductList.get(0).product_meta!!.SIDES != null)
                st_summarysides = mUPSProductList.get(0).product_meta!!.SIDES!!
            if (mUPSProductList.get(0).product_meta!!.PAPER_OR_STOCK != null)
                st_summarypaperstock = mUPSProductList.get(0).product_meta!!.PAPER_OR_STOCK!!
            if (mUPSProductList.get(0).product_meta!!.COATING != null)
                st_summarycoating = mUPSProductList.get(0).product_meta!!.COATING!!
            if (mUPSProductList.get(0).product_meta!!.PAGES != null)
                st_summarypages = mUPSProductList.get(0).product_meta!!.PAGES!!
            if (mUPSProductList.get(0).product_meta!!.COLOR_CRITICAL != null)
                st_summarycolorcritical = mUPSProductList.get(0).product_meta!!.COLOR_CRITICAL!!
            if (mUPSProductList.get(0).product_meta!!.COVER_PAPER_TYPE != null)
                st_summarycoverpapertype = mUPSProductList.get(0).product_meta!!.COVER_PAPER_TYPE!!
            if (mUPSProductList.get(0).product_meta!!.BINDING != null)
                st_summarybinding = mUPSProductList.get(0).product_meta!!.BINDING!!
            if (mUPSProductList.get(0).product_meta!!.Price_Type != null)
                st_summarypricetype = mUPSProductList.get(0).product_meta!!.Price_Type!!

            /*--------------------------------------------*/
            /*if(mUPSProductList.get(0).design_comments!= null && mUPSProductList.get(0).design_comments!!.custom_design!= null)
                st_custom_design = mUPSProductList.get(0).design_comments!!.custom_design!!
            if(mUPSProductList.get(0).design_comments!= null && mUPSProductList.get(0).design_comments!!.design_instructions!= null)
                st_design_instructions = mUPSProductList.get(0).design_comments!!.design_instructions!!
            if(mUPSProductList.get(0).design_comments!= null && mUPSProductList.get(0).design_comments!!.design_notes!= null)
                st_sdesign_notes = mUPSProductList.get(0).design_comments!!.design_notes!!
            if(mUPSProductList.get(0).design_comments!= null && mUPSProductList.get(0).design_comments!!.design_back_text!= null)
                st_design_back_text = mUPSProductList.get(0).design_comments!!.design_back_text!!
            if(mUPSProductList.get(0).design_comments!= null && mUPSProductList.get(0).design_comments!!.design_front_text!= null)
                st_design_front_text = mUPSProductList.get(0).design_comments!!.design_front_text!!*/


            //coupon object
            if (json2.contains("coupon") && json2.contains("offercode") && mUPScouponList.size > 0) {
                if (mUPScouponList.get(0).crazy_fast_price != null)
                    st_crazy_fast_price = mUPScouponList.get(0).crazy_fast_price!!
                if (mUPScouponList.get(0).economy_price != null)
                    st_economy_price = mUPScouponList.get(0).economy_price!!
                if (mUPScouponList.get(0).fast_price != null)
                    st_fast_price = mUPScouponList.get(0).fast_price!!
                if (mUPScouponList.get(0).offercode != null)
                    st_offercode = mUPScouponList.get(0).offercode!!
                if (mUPScouponList.get(0).faster_price != null)
                    st_faster_price = mUPScouponList.get(0).faster_price!!
            }


        }
        et_pricing_printing.setText(pricing_value)
        et_pricing_shipping.setText(shipping_price)


        et_summaryproduct_name.setText(st_summaryproduct_name)
        et_summaryquantity.setText(st_summaryquantity)
        et_summaryexactquantity.setText(st_summaryexactquantity)
        et_summaryprintsize.setText(st_summaryprintsize)
        et_summaryexactsize.setText(st_summaryexactsize)
        et_summarysides.setText(st_summarysides)
        et_summarypaperstock.setText(st_summarypaperstock)
        et_summarycoating.setText(st_summarycoating)
        et_summarypages.setText(st_summarypages)
        et_summarycolorcritical.setText(st_summarycolorcritical)
        et_summarycoverpapertype.setText(st_summarycoverpapertype)
        et_summarybinding.setText(st_summarybinding)
        et_summarypricetype.setText(st_summarypricetype)

        val first = pricing_value.toDouble()
        val second = shipping_price.toDouble()
        val sum = first + second
        val rounded = sum.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()
        et_totalprice.text = rounded.toString()
    }

    private fun readBundleData() {
        if(intent != null ){
            billing_info_name = intent.getStringExtra(BundleDataInfo.BILLING_INFO_NAME)
            billing_address= intent.getStringExtra(BundleDataInfo.BILLING_INFO_ADDRESS)
            billing_info_address2 = intent.getStringExtra(BundleDataInfo.BILLING_INFO_ADDRESS2)
            billing_info_city = intent.getStringExtra(BundleDataInfo.BILLING_INFO_CITY)
            billing_info_state = intent.getStringExtra(BundleDataInfo.BILLING_INFO_STATE)
            billing_info_zipcode = intent.getStringExtra(BundleDataInfo.BILLING_INFO_ZIPCODE)
            billing_info_select_profile = intent.getStringExtra(BundleDataInfo.BILLING_INFO_SELECT_PROFILE)
            billing_info_new_profile = intent.getStringExtra(BundleDataInfo.BILLING_INFO_SAVE_NEW_PROFILE)
            billing_info_default_profile = intent.getStringExtra(BundleDataInfo.BILLING_INFO_SAVE_PROFILEAS_DEFAULT)


            shipping_address_name = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_NAME)
            shipping_address_company = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_COMPANY_NAME)
            shiping_address = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_ADDRESS)
            shipping_address_address2 = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_ADDRESS2)
            shipping_address_city = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_CITY)
            shipping_address_state = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_STATE)
            shipping_address_zipcode = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_ZIPCODE)
            shiping_address_type = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_TYPE)
            shipping_address_select_profile = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_SELECT_PROFILE)
            shipping_address_new_profile = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_SAVE_NEW_PROFILE)
            shipping_address_default_profile = intent.getStringExtra(BundleDataInfo.SHIPPING_ADDRESS_SAVE_PROFILEAS_DEFAULT)


            shipping_info_email = intent.getStringExtra(BundleDataInfo.SHIPPING_INFO_EMAIL)
            shiping_cc_refer = intent.getStringExtra(BundleDataInfo.SHIPPING_INFO_CC_REFER)
            shipping_blind_shipping_check = intent.getStringExtra(BundleDataInfo.SHIPPING_INFO_BLIND_SHIPPING)


            shipping_method_name = intent.getStringExtra(BundleDataInfo.SHIPPING_METHOD)
            shipping_price = intent.getStringExtra(BundleDataInfo.SHIPPING_METHOD_ITEM_PRICE)
            shipping_delivery = intent.getStringExtra(BundleDataInfo.SHIPPING_METHOD_ITEM_DELIVERYMETHOD)
            //intent.getStringExtra(BundleDataInfo.SHIPPING_METHOD_ITEM)

            //GET SHIIPING DATA
            upload_notes = intent.getStringExtra(BundleDataInfo.DESIGN_NOTES)
            upload_instruction = intent.getStringExtra(BundleDataInfo.DESIGN_INSTRUCTION)
            upload_front = intent.getStringExtra(BundleDataInfo.DESIGN_FRONT)
            upload_back = intent.getStringExtra(BundleDataInfo.DESIGN_BACK)
        }
    }

    private fun dialogInitialization() {

        val list:ArrayList<String> = ArrayList<String>()
        if(sessionManager.states!=null) {
            list.addAll(0, sessionManager.states!!)
            Log.w("States", "*********** " + list)
        }

        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)

        list_items_list = dialog_list!!.findViewById(R.id.list_languages)

        tv_shipping_addresstype.setOnClickListener(View.OnClickListener {
            if (!dialog_list!!.isShowing())
                dialog_list!!.show()
            val state_array_adapter = ArrayAdapter<String>(this@ReviewActivity, android.R.layout.simple_spinner_item, addresstype)
            list_items_list!!.setAdapter(state_array_adapter)
            list_items_list!!.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                var address_type = list_items_list!!.getItemAtPosition(i).toString()
                tv_shipping_addresstype.setText(address_type)
                dialog_list!!.dismiss()
            })
        })
        tv_billing_state.setOnClickListener(View.OnClickListener {
            if (sessionManager.states != null) {

                if (!dialog_list!!.isShowing())
                    dialog_list!!.show()
                val state_array_adapter = ArrayAdapter<String>(this@ReviewActivity, android.R.layout.simple_spinner_item, list)
                list_items_list!!.setAdapter(state_array_adapter)
                list_items_list!!.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                    var address_type = list_items_list!!.getItemAtPosition(i).toString()
                    tv_billing_state.setText(address_type)
                    dialog_list!!.dismiss()
                })
            }
        })
        tv_shipping_state.setOnClickListener(View.OnClickListener {
            if (sessionManager.states != null) {
                if (!dialog_list!!.isShowing())
                    dialog_list!!.show()
                val state_array_adapter = ArrayAdapter<String>(this@ReviewActivity, android.R.layout.simple_spinner_item,list)
                list_items_list!!.setAdapter(state_array_adapter)
                list_items_list!!.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                    var address_type = list_items_list!!.getItemAtPosition(i).toString()
                    tv_shipping_state.setText(address_type)
                    dialog_list!!.dismiss()
                })
            }

        })

    }

    private fun initialization() {
        //footer of layout
        ll_order = findViewById(R.id.ll_order) as LinearLayout
        ll_upload = findViewById(R.id.ll_upload) as LinearLayout
        ll_shipping = findViewById(R.id.ll_shipping) as LinearLayout
        ll_review = findViewById(R.id.ll_review) as LinearLayout

        //title of item layout
        ll_ordersummary = findViewById(R.id.ll_billinginfor) as LinearLayout
        ll_billing = findViewById(R.id.ll_billing_information) as LinearLayout
        ll_shipping_order = findViewById(R.id.ll_shippinginformation) as LinearLayout
        ll_pricing = findViewById(R.id.ll_pricing_information) as LinearLayout

        //details info of each item layout
        ll_ordersummary_info = findViewById(R.id.ll_order_summary_detailview) as LinearLayout
        ll_billing_info = findViewById(R.id.ll_billing_info_details) as LinearLayout
        ll_shipping_order_info = findViewById(R.id.ll_shippinginformation_detailview) as LinearLayout
        ll_pricing_info = findViewById(R.id.ll_pricing_info_details) as LinearLayout

        //edit of each item header layout
        tv_ordersummary_edit = findViewById(R.id.tv_ordersummary_edit) as TextView
        tv_billing_edit = findViewById(R.id.tv_billing_edit) as TextView
        tv_shipping_edit = findViewById(R.id.tv_shipping_edit) as TextView
        tv_pricing_edit = findViewById(R.id.tv_pricing_edit) as TextView

        //summary components
        et_summaryproduct_name = findViewById(R.id.et_summaryproduct_name) as EditText
        et_summaryquantity = findViewById(R.id.et_summaryquantity) as EditText
        et_summaryexactquantity = findViewById(R.id.et_summaryexactquantity) as EditText
        et_summaryprintsize = findViewById(R.id.et_summaryprintsize) as EditText
        et_summaryexactsize = findViewById(R.id.et_summaryexactsize) as EditText
        et_summarysides = findViewById(R.id.et_summarysides) as EditText
        et_summarypaperstock = findViewById(R.id.et_summarypaperstock) as EditText
        et_summarycoating = findViewById(R.id.et_summarycoating) as EditText
        et_summarypages = findViewById(R.id.et_summarypages) as EditText
        et_summarycolorcritical = findViewById(R.id.et_summarycolorcritical) as EditText
        et_summarycoverpapertype = findViewById(R.id.et_summarycoverpapertype) as EditText
        et_summarybinding = findViewById(R.id.et_summarybinding) as EditText
        et_summarypricetype = findViewById(R.id.et_summarypricetype) as EditText




        //billing components
        tv_billing_name = findViewById(R.id.et_billing_name) as EditText
        tv_billing_address = findViewById(R.id.et_billing_address) as EditText
        tv_billing_address2 = findViewById(R.id.et_billing_address_2) as EditText
        tv_billing_city = findViewById(R.id.et_billing_city) as EditText
        tv_billing_state = findViewById(R.id.et_billinginfo_state) as CustomTextView
        tv_billing_zipcode = findViewById(R.id.et_billing_zipcode) as EditText

        //button
        tv_billing_btn_submit = findViewById(R.id.btn_billing_info_submit) as Button
        tv_shipping_btn_submit = findViewById(R.id.btn_shipping_info_submit) as Button
        btn_previous_screen = findViewById(R.id.btn_previousstep) as Button
        btn_submit_order = findViewById(R.id.btn_submitorder) as Button
        btn_save_order = findViewById(R.id.btn_saveorder) as Button

        //shipping components
        tv_shipping_method = findViewById(R.id.et_shipping_method) as EditText
        tv_shipping_name = findViewById(R.id.et_shipping_name) as EditText
        tv_shipping_company = findViewById(R.id.et_shipping_company) as EditText
        tv_shipping_address = findViewById(R.id.et_shipping_address) as EditText
        tv_shipping_addresstype = findViewById(R.id.et_shippinginfo_address_type) as CustomTextView
        tv_shipping_state = findViewById(R.id.et_shippinginfo_state) as CustomTextView
        tv_shipping_city = findViewById(R.id.et_shipping_city) as EditText
        tv_shipping_zipcode = findViewById(R.id.et_shippinginfo_zipcode) as EditText

        et_pricing_printing = findViewById(R.id.et_pricing_printing) as TextView
        et_pricing_shipping = findViewById(R.id.et_pricing_shipping) as TextView
        et_totalprice = findViewById(R.id.et_totalprice) as TextView

        //pricign components



        //default layout showing
        ll_ordersummary_info.visibility = View.VISIBLE

        //bottom footer listeners
        ll_upload.setOnClickListener(this)
        ll_shipping.setOnClickListener(this)
        ll_review.setOnClickListener(this)
        ll_order.setOnClickListener(this)

        //title header item click listeners
        ll_ordersummary.setOnClickListener(View.OnClickListener {
            ll_ordersummary_info.visibility = View.VISIBLE
            ll_billing_info.visibility = View.GONE
            ll_shipping_order_info.visibility = View.GONE
            ll_pricing_info.visibility = View.GONE

            tv_billing_edit.visibility= View.INVISIBLE
            tv_shipping_edit.visibility= View.INVISIBLE

        })
        ll_billing.setOnClickListener(View.OnClickListener {
            ll_ordersummary_info.visibility = View.GONE
            ll_billing_info.visibility = View.VISIBLE
            ll_shipping_order_info.visibility = View.GONE
            ll_pricing_info.visibility = View.GONE
        })
        ll_shipping_order.setOnClickListener(View.OnClickListener {
            ll_ordersummary_info.visibility = View.GONE
            ll_billing_info.visibility = View.GONE
            ll_shipping_order_info.visibility = View.VISIBLE
            ll_pricing_info.visibility = View.GONE
        })
        ll_pricing.setOnClickListener(View.OnClickListener {
            ll_ordersummary_info.visibility = View.GONE
            ll_billing_info.visibility = View.GONE
            ll_shipping_order_info.visibility = View.GONE
            ll_pricing_info.visibility = View.VISIBLE

            tv_billing_edit.visibility= View.INVISIBLE
            tv_shipping_edit.visibility= View.INVISIBLE
        })
        //
        tv_billing_btn_submit.isEnabled=false
        tv_shipping_btn_submit.isEnabled=false

        //billing edit options
        tv_billing_edit.setOnClickListener(View.OnClickListener {

            tv_billing_name.isEnabled=true
            tv_billing_address.isEnabled=true
            tv_billing_address2.isEnabled=true
            tv_billing_city.isEnabled=true
            tv_billing_state.isClickable=true
            tv_billing_zipcode.isEnabled=true
            tv_billing_btn_submit.isEnabled=true


            tv_billing_edit.visibility= View.INVISIBLE

            ll_ordersummary_info.visibility = View.GONE
            ll_billing_info.visibility = View.VISIBLE
            ll_shipping_order_info.visibility = View.GONE
            ll_pricing_info.visibility = View.GONE

            tv_shipping_edit.visibility= View.INVISIBLE


        })
        //billing save button
        tv_billing_btn_submit.setOnClickListener(View.OnClickListener {
            tv_billing_name.isEnabled=false
            tv_billing_address.isEnabled=false
            tv_billing_address2.isEnabled=false
            tv_billing_city.isEnabled=false
            tv_billing_state.isClickable=false
            tv_billing_zipcode.isEnabled=false
            tv_billing_btn_submit.isEnabled=false

            tv_billing_edit.visibility= View.INVISIBLE

            ll_ordersummary_info.visibility = View.GONE
            ll_billing_info.visibility = View.GONE
            ll_shipping_order_info.visibility = View.GONE
            ll_pricing_info.visibility = View.GONE

        })

        //shipping edit options
        tv_shipping_edit.setOnClickListener(View.OnClickListener {
            tv_shipping_method.isEnabled=true
            tv_shipping_name.isEnabled=true
            tv_shipping_company.isEnabled=true
            tv_shipping_address.isEnabled=true
            tv_shipping_addresstype.isEnabled=true
            tv_shipping_state.isClickable=true
            tv_shipping_city.isEnabled=true
            tv_shipping_zipcode.isEnabled=true

            tv_shipping_btn_submit.isEnabled=true

            tv_shipping_edit.visibility= View.INVISIBLE

            ll_ordersummary_info.visibility = View.GONE
            ll_billing_info.visibility = View.GONE
            ll_shipping_order_info.visibility = View.VISIBLE
            ll_pricing_info.visibility = View.GONE

            tv_billing_edit.visibility= View.INVISIBLE

        })

        //shipping save button
        tv_shipping_btn_submit.setOnClickListener(View.OnClickListener {
            tv_shipping_method.isEnabled=true
            tv_shipping_name.isEnabled=true
            tv_shipping_company.isEnabled=true
            tv_shipping_address.isEnabled=true
            tv_shipping_addresstype.isEnabled=true
            tv_shipping_state.isClickable=false
            tv_shipping_city.isEnabled=true
            tv_shipping_zipcode.isEnabled=true

            tv_shipping_btn_submit.isEnabled=false

            tv_shipping_edit.visibility= View.INVISIBLE

            ll_ordersummary_info.visibility = View.GONE
            ll_billing_info.visibility = View.GONE
            ll_shipping_order_info.visibility = View.GONE
            ll_pricing_info.visibility = View.GONE

        })
        //previous screen
        btn_previous_screen.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        //save order
        btn_save_order.setOnClickListener(View.OnClickListener {
            if(ConnectionManager.checkConnection(applicationContext)) {
                saveOrder()
            }
            else{
                ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
            }
        })
        //submit order
        btn_submit_order.setOnClickListener(View.OnClickListener {
            if(ConnectionManager.checkConnection(applicationContext)) {
                saveOrder()
            }
            else{
                ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
            }

        })

    }


    private fun saveOrder() {

        val filelist:ArrayList<String> = ArrayList<String>()
        val sublabellist: ArrayList<String> = ArrayList<String>()
        val extraschargelist: ArrayList<String> = ArrayList<String>()
        val addoptionallist: ArrayList<String> = ArrayList<String>()
        val addoptionalsublist: ArrayList<String> = ArrayList<String>()
        var extralen:Int=1
        var addoptlen:Int=1
        var sublen:Int=1
        try {

            if(sessionManager.fileuploadimagelist!=null) {
                filelist.addAll(0, sessionManager.fileuploadimagelist!!)
                Log.w("files", "*********** " + filelist)
            }
            
            val json = sessionManager.getobject
            Log.w("Review:::", "Data::" + json)

            val data_string = GsonBuilder().setPrettyPrinting().create().toJson(sessionManager.getobject)
            Log.w("Review::::", "Data2::" + data_string)

            val parser = JSONParser()
            val jsondata = parser.parse(json) as JSONObject
            Log.w("Review::::", "Data23::" + jsondata.toString())
          //  sessionManager.parser(jsondata.toString())




       //     fun parser(extracharge: String) {
                val jsonobj = org.json.JSONObject(jsondata.toString())
                if(jsondata.toString().contains("sublabels")) {
                    val product = jsonobj.getJSONObject("product")
                    val mainObject2 = product.getJSONObject("sublabels")
                    sublen = mainObject2.length()
                    val data_keys = mainObject2.keys()

                    while (data_keys.hasNext()) {
                        val key = data_keys.next()
                        val value = mainObject2.optString(key)
                        Log.w("Review  session2", "key:" + key + "--Value::" + value)
                        sublabellist.add(key+":"+value)
                        Log.w("Review  sublabellist", "list:"+sublabellist)
                }
                }
                if(jsondata.toString().contains("ExtraCharges")) {
                    val product = jsonobj.getJSONObject("product")
                    val mainObject = product.getJSONArray("ExtraCharges")
                    extralen = mainObject.length()
                    for (i in 0 until mainObject.length()) {
                        val uniObject = mainObject.getJSONObject(i)
                        val data_keys = uniObject.keys()
                        val extraschargesublist: ArrayList<String> = ArrayList<String>()

                        while (data_keys.hasNext()) {
                            val key = data_keys.next()
                            val value = uniObject.optString(key)
                            Log.w("Review  session1", "key:" + key + "--Value::" + value)
                            extraschargesublist.add(key+":"+value)
                            Log.w("Reviewextraschargelist", "list:"+extraschargesublist)
                        }
                        extraschargelist.addAll(i,extraschargesublist)
                    }
                }
                if(jsondata.toString().contains("AddOptionals")) {
                    val product = jsonobj.getJSONObject("product")
                    val mainObject3 = product.getJSONArray("AddOptionals")
                    addoptlen = mainObject3.length()
                    for (i in 0 until mainObject3.length()) {
                        val uniObject = mainObject3.getJSONObject(i)
                        val data_keys = uniObject.keys()

                        while (data_keys.hasNext()) {
                            val key = data_keys.next()
                            val value = uniObject.optString(key)
                            Log.w("Review  session3", "key:" + key + "--Value::" + value)
                            addoptionalsublist.add(key+":"+value)
                            Log.w("Review  addoptionallist", "list:"+addoptionalsublist)
                        }
                        addoptionallist.addAll(i,addoptionalsublist)
                    }
                }

          //  }

        }catch (e:Exception){
            Log.w("Review::::", "Exception::" + e.toString())
        }



        val client = AsyncHttpClient()
        client.addHeader("encryption_key", ApiInterface.ENCRIPTION_KEY)
        val params = RequestParams()
        val jsonObject = JSONObject()


        val productobject = JSONObject()
        productobject.put("productid", productid)
        productobject.put("pricingid", pricingid)
        productobject.put("productname", st_summaryproduct_name)
        productobject.put("paper_type", st_paper_type)

        val productsubobj = JSONObject()
        productsubobj.put("Quantity",st_summaryquantity)
        productsubobj.put("Exact_Quantity",st_summaryexactquantity)
        productsubobj.put("PRINT_SIZE",st_summaryprintsize)
        productsubobj.put("EXACT_SIZE",st_summaryexactsize)
        productsubobj.put("SIDES",st_summarysides)
        productsubobj.put("PAPER_OR_STOCK",st_summarypaperstock)
        productsubobj.put("COATING",st_summarycoating)
        productsubobj.put("PAGES",st_summarypages)
        productsubobj.put("COLOR_CRITICAL",st_summarycolorcritical)
        productsubobj.put("COVER PAPER TYPE",st_summarycoverpapertype)
        productsubobj.put("BINDING",st_summarybinding)
        productsubobj.put("Price_Type",st_summarypricetype)
        productsubobj.put("Spot_UV_Gloss_Coating","")
        productsubobj.put("Variable_Data","")
        productsubobj.put("Lamination","")
        productsubobj.put("Corner_Rounding","")
        productobject.put("product_meta",productsubobj)


        val jsonArray = JSONArray()

        for (image in 0 until extraschargelist.size) {
            val imageJson = JSONObject()
            val position: Int = extraschargelist.get(image).indexOf(":")
            val key: String = extraschargelist.get(image).substring(0, position).replace("\\\"", "")
            val value: String = extraschargelist.get(image).substring(position + 1).replace("\\\"", "")
            imageJson.put(key, value)
            jsonArray.put(imageJson)

        }
        productobject.put("ExtraCharges", jsonArray)


        val jsonArray1 = JSONArray()
        var imageJson = JSONObject()

        for (image in 0 until addoptionallist.size) {

            val position:Int = addoptionallist.get(image).indexOf(":")
            val key:String = addoptionallist.get(image).substring(0, position).replace("\\\"","")
            val value:String = addoptionallist.get(image).substring(position+1).replace("\\\"","")

            imageJson.put(key,value)

            jsonArray1.put(imageJson)
        }
        productobject.put("AddOptionals", jsonArray1)

       /* var tmp=0
        var tmp2=1
        for (image in 0 until addoptlen) {

            for (i in tmp until addoptionallist.size) {
                if ((i/tmp2)!=addoptlen) {
                    val position: Int = addoptionallist.get(i).indexOf(":")
                    val key: String = addoptionallist.get(i).substring(0, position).replace("\\\"", "")
                    val value: String = addoptionallist.get(i).substring(position + 1).replace("\\\"", "")

                    imageJson.put(key, value)
                }else {

                    // jsonArray1.put(imageJson)
                    tmp = i
                    tmp2 = tmp2+1
                    jsonArray1.put(imageJson)
                    imageJson = JSONObject()
                }
            }

        }
        productobject.put("AddOptionals", jsonArray1)*/




        val jsonArray2 = JSONArray()
        for (image in 0 until sublabellist.size) {
            val imageJson = JSONObject()
            val position:Int = sublabellist.get(image).indexOf(":")
            val key:String = sublabellist.get(image).substring(0, position).replace("\\\"","")
            val value:String = sublabellist.get(image).substring(position+1).replace("\\\"","")

            imageJson.put(key,value)

            jsonArray2.put(imageJson)
        }
        productobject.put("sublabels", jsonArray2)


        productobject.put("Price", pricing_value)
        productobject.put("Price_type", Price_type)

        jsonObject.put("product",productobject)
        jsonObject.put("zipcode",ups_zipcode)
        jsonObject.put("upsprice",upsprice)



        val couponobject = JSONObject()
        couponobject.put("economy_price",st_economy_price)
        couponobject.put("fast_price",st_fast_price)
        couponobject.put("faster_price",st_faster_price)
        couponobject.put("crazy_fast_price",st_crazy_fast_price)
        couponobject.put("offercode",st_offercode)
        jsonObject.put("coupon",couponobject)


        val fileobject = JSONObject()
        for (image in 1 until filelist.size) {
            fileobject.put(image, filelist.get(image))
            //fileobject.put("2", "1314372231526041491.png")
        }
        jsonObject.put("files",fileobject)


        val designing = JSONObject()
        designing.put("design_instructions",upload_instruction)
        designing.put("design_front_text", upload_front)
        designing.put("design_back_text", upload_back)
        designing.put("design_notes", upload_notes)
        designing.put("design_notes", upload_notes)
        designing.put("custom_design",st_custom_design)
        jsonObject.put("design_comments",designing)

        jsonObject.put("userID","58")

        val jsonObject5 = JSONObject()

        val shippingobj = JSONObject()
        shippingobj.put("select_profile",shipping_address_select_profile)
        shippingobj.put("address_type",tv_shipping_addresstype.text.toString())
        shippingobj.put("company",tv_shipping_company.text.toString())
        shippingobj.put("name",tv_shipping_name.text.toString())
        shippingobj.put("address",tv_shipping_address.text.toString())
        shippingobj.put("address2",shipping_address_address2)
        shippingobj.put("city",tv_billing_city.text.toString())
        shippingobj.put("state",tv_shipping_state.text.toString())
        shippingobj.put("zipcode",tv_shipping_zipcode.text.toString())
        shippingobj.put("shipping_instructions","")
        if(shipping_address_new_profile.equals("true"))
            shipping_address_new_profile="1"
        shippingobj.put("new_profile",shipping_address_new_profile)
        if(shipping_address_default_profile.equals("true"))
            shipping_address_default_profile="1"
        shippingobj.put("shipping_default_profile",shipping_address_default_profile)
        shippingobj.put("alternate_email",shipping_info_email)
        shippingobj.put("customer_referrence",shiping_cc_refer)
        shippingobj.put("delivery_method",shipping_delivery)
        shippingobj.put("rate_estimate_id","")
        shippingobj.put("shipping_method",shipping_method_name)
        shippingobj.put("shipping_amount",shipping_price)
        if(shipping_blind_shipping_check.equals("true"))
            shipping_blind_shipping_check="1"
        shippingobj.put("blind_shipping",shipping_blind_shipping_check)

        jsonObject5.put("shipping",shippingobj)

        val billingobj = JSONObject()
        billingobj.put("select_profile",billing_info_select_profile)
        billingobj.put("billing_name",tv_billing_name.text.toString())
        billingobj.put("address",tv_billing_address.text.toString())
        billingobj.put("address2",tv_billing_address2.text.toString())
        billingobj.put("city",tv_billing_city.text.toString())
        billingobj.put("state",tv_billing_state.text.toString())
        billingobj.put("zipcode",tv_billing_zipcode.text.toString())
        if(billing_info_new_profile.equals("true"))
            billing_info_new_profile="1"
        billingobj.put("new_profile",billing_info_new_profile)
        if(billing_info_default_profile.equals("true"))
            billing_info_default_profile="1"
        billingobj.put("billing_default_profile",billing_info_default_profile)

        jsonObject5.put("billing",billingobj)


        jsonObject.put("info",jsonObject5)


        Log.e("RESULT",jsonObject.toString())

        val savobj = JSONObject()
        savobj.put("saveOrder",jsonObject)
        Log.w("Main","SUccess  "+savobj)
        SendDeviceDetails(this).execute(url, savobj.toString())
    }




    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.ll_order -> {
            }
            R.id.ll_shipping -> {
            }
            R.id.ll_review -> {

            }
            R.id.ll_upload -> {
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }




    var url = ApiInterface.BASE_URL + "/Mobile/saveOrder"

    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }



    
    
    

}

