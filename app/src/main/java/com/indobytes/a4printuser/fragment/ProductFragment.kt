package com.indobytes.a4printuser.fragment

import android.app.Dialog
import android.app.Fragment
import android.app.SharedElementCallback
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.GridView
import android.widget.ProgressBar
import android.widget.Toast
import com.indobytes.a4printuser.Helper.ConnectionManager
import com.indobytes.a4printuser.R
import com.indobytes.a4printuser.adapter.ProductDetailsAdapter
import com.indobytes.a4printuser.model.APIResponse.ProductsResponse
import com.indobytes.a4printuser.model.APIResponse.ProductsResponseDetails
import com.indobytes.a4printuser.model.ApiInterface
import kotlinx.android.synthetic.main.app_bar_main.*
import retrofit2.Call
import retrofit2.Callback

class ProductFragment : Fragment() {

    private var mProductGridView: GridView? = null
    public var mProductList: ArrayList<ProductsResponseDetails> = ArrayList<ProductsResponseDetails>()
    private var mProductDetailsAdapter : ProductDetailsAdapter? = null
    private var mProgressBar: ProgressBar? = null
    //2
    companion object {

        fun newInstance(): ProductFragment {
            return ProductFragment()
        }
    }

    //3
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?  {
        getActivity().setTitle("PRODUCTS LIST")

        val rootView = LayoutInflater.from(this.activity).inflate(R.layout.activity_products_list, null, false)
        mProductGridView = rootView!!.findViewById(R.id.products_list_gridview) as GridView
        myloading()
        if(ConnectionManager.checkConnection(this.activity)) {
            callProductsDetails()
        }else{
            Toast.makeText(this.activity,resources.getString(R.string.network_error),Toast.LENGTH_SHORT).show()
        }
        return rootView

    }
    private fun callProductsDetails() {
        my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.getProductDetails(ApiInterface.ENCRIPTION_KEY)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ProductsResponse> {
            override fun onResponse(call: Call<ProductsResponse>, response: retrofit2.Response<ProductsResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    var list: List<ProductsResponseDetails> = response.body()!!.data!!

                    for (item: ProductsResponseDetails in list.iterator()) {
                        mProductList.add(item)
                        setProductAdapter(mProductList)
                    }

                }
            }

            override fun onFailure(call: Call<ProductsResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }

    private fun setProductAdapter(mProductList: ArrayList<ProductsResponseDetails>) {
        mProductDetailsAdapter = ProductDetailsAdapter(mProductList, this.activity)
        mProductGridView!!.adapter = mProductDetailsAdapter
        mProductDetailsAdapter!!.notifyDataSetChanged()
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this.activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

}


