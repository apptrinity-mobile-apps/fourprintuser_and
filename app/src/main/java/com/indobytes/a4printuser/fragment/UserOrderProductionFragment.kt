package com.indobytes.a4printuser.fragment

import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.Helper.ConnectionManager

import com.indobytes.a4printuser.R
import com.indobytes.a4printuser.adapter.OrderDetailsAdapter
import com.indobytes.a4printuser.model.APIResponse.UserOrderInfo
import com.indobytes.a4printuser.model.APIResponse.UserOrderResponse
import com.indobytes.a4printuser.model.ApiInterface
import retrofit2.Call
import retrofit2.Callback

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [UserOrderFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [UserOrderFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class UserOrderProductionFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var type: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var status_type:Int?=null
    private var no_info: TextView? = null

    private var mOrderListView: ListView? = null
    public var mOrderInfo: ArrayList<UserOrderInfo> = ArrayList<UserOrderInfo>()

    private var mUserOrderINfoAdapter : OrderDetailsAdapter? = null
    internal lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            type = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        // Inflate the layout for this fragment
        val rootView = LayoutInflater.from(this.activity).inflate(R.layout.fragment_userorders, null, false)
        mOrderListView = rootView!!.findViewById(R.id.product_list_listView) as ListView
        no_info = rootView!!.findViewById(R.id.no_info) as TextView
        no_info!!.visibility = View.GONE
        mOrderListView!!.visibility = View.VISIBLE
        sessionManager = SessionManager(this.activity!!)
        myloading()
        if(ConnectionManager.checkConnection(this.activity!!)) {
            callUserOrderAPI(rootView,type)
        }
        else{
            Toast.makeText(this.activity,resources.getString(R.string.network_error), Toast.LENGTH_SHORT).show()
        }

        return rootView
    }

    private fun callUserOrderAPI(rootView: View, type: String?) {
        val apiService = ApiInterface.create()
        Log.w("Fragment","****"+type)
        my_loader.show()
        val call = apiService.getUserOrders(ApiInterface.ENCRIPTION_KEY,sessionManager.isUserId,"IN PRODUCTION")
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UserOrderResponse> {
            override fun onResponse(call: Call<UserOrderResponse>, response: retrofit2.Response<UserOrderResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    if(response.body()!!.data !=null) {
                    var list: List<UserOrderInfo> = response.body()!!.data!!
                    Log.w("Result_Address_Order", response.body()!!.status!!)

                        for (item: UserOrderInfo in list.iterator()) {
                            mOrderInfo.add(item)
                            setOrderAdapter(mOrderInfo)
                        }
                    }else{
                        no_info!!.visibility = View.VISIBLE
                        mOrderListView!!.visibility = View.GONE
                    }

                }
            }

            override fun onFailure(call: Call<UserOrderResponse>, t: Throwable) {
                Log.w("Result_Address_Order",t.toString())
            }
        })
    }

    private fun setOrderAdapter(mOrderInfo: ArrayList<UserOrderInfo>) {
        if (activity != null && !activity!!.isFinishing()) {
            mUserOrderINfoAdapter = OrderDetailsAdapter(mOrderInfo, this.activity!!)
            mOrderListView!!.adapter = mUserOrderINfoAdapter
            mUserOrderINfoAdapter!!.notifyDataSetChanged()
        }

    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment UserOrderFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                UserOrderProductionFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }



    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this.activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(true);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
    override fun onPause() {
        my_loader.dismiss()
        super.onPause()
    }
}
