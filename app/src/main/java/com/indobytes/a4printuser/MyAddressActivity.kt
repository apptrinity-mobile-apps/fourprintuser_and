package com.indobytes.a4printuser

import android.app.Dialog
import android.app.Fragment
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.Helper.ConnectionManager
import com.indobytes.a4printuser.model.APIResponse.AddressesResponse
import com.indobytes.a4printuser.model.APIResponse.AddressesResponseDetails
import com.indobytes.a4printuser.model.APIResponse.DeleteAddressesResponse
import com.indobytes.a4printuser.model.ApiInterface
import kotlinx.android.synthetic.main.app_bar_main.*
import retrofit2.Call
import retrofit2.Callback

class MyAddressActivity : AppCompatActivity() {
    private var rv_address_id: RecyclerView? = null
    private var tv_new_address:TextView? = null
    public var mAddressList: ArrayList<AddressesResponseDetails> = ArrayList<AddressesResponseDetails>()
    internal lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_myaddress)
        sessionManager = SessionManager(this)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        var rootview = findViewById(R.id.rootview) as RelativeLayout
        toolbar.setNavigationOnClickListener {
            val intent = Intent(this@MyAddressActivity,MainActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
        toolbar.setTitle("MY ADDRESS")
        myloading()


        rv_address_id = findViewById(R.id.rv_address_id) as RecyclerView
        rv_address_id!!.setNestedScrollingEnabled(false)
        tv_new_address = findViewById(R.id.tv_new_address) as TextView
        tv_new_address!!.setOnClickListener {
            val intent = Intent(this, AddressEditActivity::class.java)
            intent.putExtra("address_type", "new")
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
        if(ConnectionManager.checkConnection(applicationContext)) {
            callAddressesDetailsAPI()
        }
        else{
            ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
        }


    }
    private fun callAddressesDetailsAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.getAddresses(ApiInterface.ENCRIPTION_KEY,sessionManager.isUserId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<AddressesResponse> {
            override fun onResponse(call: Call<AddressesResponse>, response: retrofit2.Response<AddressesResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    var list: Array<AddressesResponseDetails>? = response.body()!!.data
                    Log.w("Result_Address","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1")) {
                        for (item: AddressesResponseDetails in list!!.iterator()) {
                            mAddressList.add(item)
                            setAddressesAdapter(mAddressList)
                        }
                    }
                }
            }
            override fun onFailure(call: Call<AddressesResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }

    fun setAddressesAdapter(mAddressList: ArrayList<AddressesResponseDetails>) {
        val dAdapter = DefaultBookAdapter(this, mAddressList)
        val mLayoutManager = LinearLayoutManager(this)
        rv_address_id!!.setLayoutManager(mLayoutManager)
        rv_address_id!!.setItemAnimator(DefaultItemAnimator())
        rv_address_id!!.setAdapter(dAdapter)
        dAdapter.notifyDataSetChanged()

    }

    override fun onBackPressed() {
        val intent = Intent(this@MyAddressActivity,MainActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)

        super.onBackPressed()
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    internal class DefaultBookAdapter(private val mContext: Context, var address_responseInfo: java.util.ArrayList<AddressesResponseDetails>) : RecyclerView.Adapter<DefaultBookAdapter.MyViewHolder>() {

        inner class MyViewHolder(rowView: View) : RecyclerView.ViewHolder(rowView) {
            internal var tv_name_id: TextView
            internal var tv_company_id: TextView
            internal var tv_address_id: TextView
            internal var address_two_id: TextView
            internal var tv_city_id: TextView
            internal var tv_state_id: TextView
            internal var tv_pincode_id: TextView
            internal var tv_edit_profile: TextView
            internal var tv_delete_profile: TextView
            internal var ll_address_id: LinearLayout


            init {
                tv_name_id = rowView.findViewById(R.id.tv_name_id) as TextView
                tv_company_id = rowView.findViewById(R.id.tv_company_id) as TextView
                tv_address_id = rowView.findViewById(R.id.tv_address_id) as TextView
                address_two_id = rowView.findViewById(R.id.address_two_id) as TextView
                tv_city_id = rowView.findViewById(R.id.tv_city_id) as TextView
                tv_state_id = rowView.findViewById(R.id.tv_state_id) as TextView
                tv_pincode_id = rowView.findViewById(R.id.tv_pincode_id) as TextView
                tv_edit_profile = rowView.findViewById(R.id.tv_edit_profile) as TextView
                tv_delete_profile = rowView.findViewById(R.id.tv_delete_profile) as TextView
                ll_address_id = rowView.findViewById(R.id.ll_address_id) as LinearLayout

            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.address_list_item, parent, false)
            myloading()

            return MyViewHolder(itemView)
        }


        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            try {

                holder.tv_name_id.setText(address_responseInfo[position].name)
                holder.tv_company_id.setText(address_responseInfo[position].company_name)
                holder.tv_address_id.setText(address_responseInfo[position].address1)
                holder.address_two_id.setText(address_responseInfo[position].address2)
                holder.tv_city_id.setText(address_responseInfo[position].city)
                holder.tv_state_id.setText(address_responseInfo[position].state)
                holder.tv_pincode_id.setText(address_responseInfo[position].pincode)
                if (position % 2 == 1) {
                    holder.ll_address_id.setBackgroundColor(Color.parseColor("#e8f5fb"))
                } else {
                    holder.ll_address_id.setBackgroundColor(Color.parseColor("#f9f7ea"))
                }
                holder.tv_edit_profile.setOnClickListener {
                    //deleteCustomerAddress
                    //Dialog delete_dialog=new Dialog(mContext);


                    val intent = Intent(mContext, AddressEditActivity::class.java)
                    intent.putExtra("address_type", "edit")
                    intent.putExtra("address_id", address_responseInfo[position].address_id)
                    intent.putExtra("name", address_responseInfo[position].name)
                    intent.putExtra("company", address_responseInfo[position].company_name)
                    intent.putExtra("address", address_responseInfo[position].address1)
                    intent.putExtra("address_two", address_responseInfo[position].address2)
                    intent.putExtra("city", address_responseInfo[position].city)
                    intent.putExtra("state", address_responseInfo[position].state)
                    intent.putExtra("pincode", address_responseInfo[position].pincode)
                    intent.putExtra("addr_type_billing", address_responseInfo[position].is_billing)
                    intent.putExtra("addr_type_shipping", address_responseInfo[position].is_shipping)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    mContext.startActivity(intent)
                }
                holder.tv_delete_profile.setOnClickListener {
                    //deleteCustomerAddress
                    val address_id:String= address_responseInfo[position].address_id.toString()
                    val name = address_responseInfo[position].name.toString()
                    val company_name:String= address_responseInfo[position].company_name.toString()
                    val address1:String= address_responseInfo[position].address1.toString()
                    val address2:String= address_responseInfo[position].address2.toString()
                    val city:String= address_responseInfo[position].city.toString()
                    val state:String= address_responseInfo[position].state.toString()
                    val zipcode:String= address_responseInfo[position].pincode.toString()
                    if (address_id!= null) {
                        if(ConnectionManager.checkConnection(mContext)) {
                            deleteAddressAPI(address_id, name, company_name, address1, address2, city, state, zipcode)
                        } else{
                            Toast.makeText(mContext,mContext.resources.getString(R.string.network_error),Toast.LENGTH_SHORT).show()
                        }

                    }
                }
            } catch (e: Exception) {
                Log.e("AdapterError", "AdapterError")
            }

        }


        private fun deleteAddressAPI(address_id: String, name: String, company_name: String, address1: String, address2: String, city: String, state: String, zipcode: String) {
            my_loader.show()
            val apiService = ApiInterface.create()
            val call = apiService.deleteAddresses(ApiInterface.ENCRIPTION_KEY,address_id,name,company_name,address1,address2,city,state,zipcode)
            Log.d("REQUEST", call.toString() + "")
            call.enqueue(object : Callback<DeleteAddressesResponse> {
                override fun onResponse(call: Call<DeleteAddressesResponse>, response: retrofit2.Response<DeleteAddressesResponse>?) {
                    my_loader.dismiss()
                    if (response != null) {
                        Log.w("Result_Address","Result : "+response.body()!!.status)
                        if(response.body()!!.status.equals("1")) {
                            val intent = Intent(mContext, MyAddressActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                            mContext.startActivity(intent)
                        }

                    }
                }

                override fun onFailure(call: Call<DeleteAddressesResponse>, t: Throwable) {
                    Log.w("Response_Product","Result : Failed")
                }
            })
        }
        var fragmentName: Fragment? = null

        override fun getItemCount(): Int {
            return address_responseInfo.size
        }
        internal lateinit var my_loader: Dialog
        private fun myloading() {
            my_loader = Dialog(mContext)
            my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
            my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            my_loader.setCancelable(false);
            my_loader.setContentView(R.layout.mkloader_dialog)
        }


    }
}


