package com.indobytes.a4printuser

import android.app.Dialog
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Html
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_order2.*
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import android.os.AsyncTask.execute
import android.view.Window
import android.widget.EditText
import com.indobytes.a4print.Session.SessionManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.app_bar_main.*


class OrderActivity : AppCompatActivity() , View.OnClickListener {


    private lateinit var toolbar: Toolbar
    var product_id: String = ""
    var product_name: String = ""
    var product_image: String = ""
    var productdesc: String = ""
    var txt_from_web: String = ""
    lateinit var iv_productdetail: ImageView
    lateinit var tv_productdetail: TextView
    lateinit var tv_productdesc: EditText

    internal lateinit var ll_order: LinearLayout
    internal lateinit var ll_upload: LinearLayout
    internal lateinit var ll_shipping: LinearLayout
    internal lateinit var ll_review: LinearLayout
    internal lateinit var my_web_view: WebView
    internal lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order2)
        sessionManager = SessionManager(this)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setTitle("ORDER")
        initialise()
        myloading()
        ll_order = findViewById(R.id.ll_order) as LinearLayout
        ll_upload = findViewById(R.id.ll_upload) as LinearLayout
        ll_shipping = findViewById(R.id.ll_shipping) as LinearLayout
        ll_review = findViewById(R.id.ll_review) as LinearLayout
        my_web_view = findViewById(R.id.my_web_view) as WebView


        ll_upload.setOnClickListener(this)
        ll_shipping.setOnClickListener(this)
        ll_review.setOnClickListener(this)
        ll_order.setOnClickListener(this)



        if (intent !=null && intent.getStringExtra("product_id")!=null) {
            product_id = intent.getStringExtra("product_id")
            product_name = intent.getStringExtra("product_name")
            product_image = intent.getStringExtra("product_image")
            productdesc = intent.getStringExtra("product_desc")

            tv_productdetail!!.setText(product_name)
            tv_productdesc.setText(Html.fromHtml(productdesc))
             product_name = product_name.replace(" ", "-")
            Log.e("product_id", product_id + "----" + product_name + "-----" + product_image)
            Picasso.with(this).load(product_image).into(iv_productdetail)
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true)
            }
        }

        my_web_view.settings.javaScriptEnabled = true
        my_web_view.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {

                    my_web_view.loadUrl("javascript:HtmlViewer.showHTML" +
                            "(document.getElementsByTagName('html')[0].innerHTML);")
            }
        }
        my_web_view.loadUrl("https://www.4print.com/product_mob/"+product_name);
        my_web_view.addJavascriptInterface(MyJavaScriptInterface(), "HtmlViewer");





    }

    private fun initialise() {

        val ll_productdetails = findViewById(R.id.ll_productdetails) as LinearLayout
        val ll_productdetail_view = findViewById(R.id.ll_productdetail_view) as LinearLayout
        val ll_pricecalculator = findViewById(R.id.ll_pricecalculator) as LinearLayout
        val ll_webview_pricecalc = findViewById(R.id.ll_webview_pricecalc) as LinearLayout
        val iv_product_detail = findViewById(R.id.iv_product_detail) as ImageView
        val iv_pricecalc = findViewById(R.id.iv_pricecalc) as ImageView


        iv_productdetail = findViewById(R.id.iv_productdetail)
        tv_productdetail = findViewById(R.id.tv_productdetail)
        tv_productdesc = findViewById(R.id.tv_productdesc)

        ll_productdetail_view.visibility = View.VISIBLE




        ll_productdetails.setOnClickListener {
            ll_productdetail_view.visibility = View.VISIBLE
            ll_webview_pricecalc.visibility = View.GONE
            iv_product_detail.setImageDrawable(resources.getDrawable(R.drawable.up_arrow_2))
            iv_pricecalc.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))


        }
        ll_pricecalculator.setOnClickListener {
            ll_webview_pricecalc.visibility = View.VISIBLE
            ll_productdetail_view.visibility = View.GONE
            iv_pricecalc.setImageDrawable(resources.getDrawable(R.drawable.up_arrow_2))
            iv_product_detail.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))
        }

        tv_productdetail.setOnClickListener {
           // val intent = Intent(this, ShippingActivity::class.java)
           // this.startActivity(intent)
        }


    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.ll_order -> {
            }
            R.id.ll_shipping -> {
            }
            R.id.ll_review -> {
            }
            R.id.ll_upload -> {
            }
        }
    }

    internal inner class MyJavaScriptInterface() {

        @JavascriptInterface
        fun showHTML(html: String) {
            println(Html.fromHtml(html).toString())

            val jtoken = JSONTokener(Html.fromHtml(html).toString()).nextValue()
            if (jtoken is JSONObject) {
                sessionManager.setUserObject(jtoken.toString(), "json_response")

                val intent = Intent(this@OrderActivity, UploadActivity::class.java)
                intent.putExtra("result", Html.fromHtml(html).toString());
                intent.putExtra("product_name", product_name);
                intent.putExtra("product_id", product_id);
                intent.putExtra("product_image", product_image);
                finish()
                startActivity(intent)
            }
            else if (jtoken is JSONArray){
                val intent = Intent(this@OrderActivity, UploadActivity::class.java)
                intent.putExtra("result", Html.fromHtml(html).toString());
                finish()
                startActivity(intent)
            }

        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {

        if (event.action === KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (my_web_view.canGoBack()) {
                        my_web_view.goBack()
                    } else {
                        finish()
                    }
                    return true
                }
            }

        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onDestroy() {
        my_web_view.removeJavascriptInterface("HtmlViewer")
        super.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
}



