package com.indobytes.a4printuser

import android.app.Dialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.indobytes.a4printuser.Helper.ConnectionManager
import com.indobytes.a4printuser.adapter.OrederInfoAdapter
import com.indobytes.a4printuser.model.APIResponse.OrderData
import com.indobytes.a4printuser.model.APIResponse.OrderDetailsResponse
import com.indobytes.a4printuser.model.APIResponse.Pricing
import com.indobytes.a4printuser.model.APIResponse.ProductMeta
import com.indobytes.a4printuser.model.ApiInterface
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import android.R.attr.bitmap
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.PendingIntent.getActivity
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.pdf.PdfDocument
import android.os.*
import android.print.*
import android.print.pdf.PrintedPdfDocument
import android.util.SparseIntArray
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import com.indobytes.a4printuser.Helper.PdfDocumentAdapter
import com.indobytes.a4printuser.adapter.MyPrintDocumentAdapter
import com.indobytes.a4printuser.adapter.OrderDetailsAdapter
import kotlinx.android.synthetic.main.app_bar_main.*
import okhttp3.internal.http2.Settings
import java.io.ByteArrayOutputStream
import java.io.FileOutputStream
import java.io.IOException
import java.util.ArrayList
import java.util.logging.Logger


class OrderDetailsActivity : AppCompatActivity() {
    internal lateinit var orderId: String

    internal lateinit var productname: String
    internal lateinit var productimage: String
    internal lateinit var  rv_OrderDetails:RecyclerView

    internal lateinit var  tv_print_price:TextView
    internal lateinit var  tv_shiping_price:TextView
    internal lateinit var  tv_tt_price:TextView
    internal lateinit var  tv_pay_status:TextView

    internal lateinit var  tv_header_productname:TextView
    internal lateinit var  tv_myorders:TextView
    internal lateinit var  tv_printer:TextView
    internal lateinit var  tv_header_productimage:ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)

        if(intent != null && intent.getStringExtra("order_id")!= null && intent.getStringExtra("product_name")!= null && intent.getStringExtra("image_url")!= null){
            orderId = intent.getStringExtra("order_id")
            productname = intent.getStringExtra("product_name")
            productimage = intent.getStringExtra("image_url")
        }
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setTitle("ORDER DETAILS")
        myloading()
        var rootview = findViewById(R.id.rootview) as RelativeLayout
        rv_OrderDetails = findViewById(R.id.rv_orderdetails_view) as RecyclerView
        rv_OrderDetails.isNestedScrollingEnabled=false

        tv_print_price = findViewById(R.id.tv_print_price) as TextView
        tv_shiping_price = findViewById(R.id.tv_shiping_price) as TextView
        tv_tt_price = findViewById(R.id.tv_total_price) as TextView
        tv_pay_status = findViewById(R.id.tv_pay_status) as TextView

        tv_header_productname = findViewById(R.id.tv_header_productname) as TextView
        tv_header_productimage = findViewById(R.id.tv_header_productimage) as ImageView
        tv_myorders = findViewById(R.id.myorders) as TextView
        tv_printer = findViewById(R.id.printer) as TextView


        if(ConnectionManager.checkConnection(this)) {
            callOrderDetailsAPI(rootview)
        }else{
            ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
        }
        tv_myorders.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        tv_printer.setOnClickListener(View.OnClickListener {
            //doPrint()
           // doprintScreen()
        })

    }

    @SuppressLint("NewApi")
    private fun doprintScreen() {

        val printManager = this.getSystemService(Context.PRINT_SERVICE) as PrintManager
        try {
            val printAdapter =  PdfDocumentAdapter(this, "");
            printManager.print("Document", printAdapter,  PrintAttributes . Builder ().build());
        } catch (e: Exception) {
            Logger.getLogger(e.toString())
        }
    }



    private fun callOrderDetailsAPI(rootview: RelativeLayout) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getOrderDetails(ApiInterface.ENCRIPTION_KEY,orderId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<OrderDetailsResponse> {
            override fun onResponse(call: Call<OrderDetailsResponse>, response: retrofit2.Response<OrderDetailsResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Order_details", response.body()!!.status)

                    if(response.body()!!.status.equals("1") && response.body()!!.data!=null) {
                        val list: Array<ProductMeta>? = response.body()!!.data!!.productMeta
                        if(list!!.size>0)
                            setDatatoAdapter(response.body()!!.data)
                        if(response.body()!!.data!!.pricing != null)
                            setPriceDatatoView(response.body()!!.data!!.pricing)

                    }

                }
            }

            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                Log.w("Result_Order_details",t.toString())
            }
        })
    }

    private fun setPriceDatatoView(pricing: Pricing?) {
        tv_print_price.setText(pricing!!.print_price)
        tv_shiping_price.setText(pricing!!.shopping_price)
        tv_tt_price.setText(pricing!!.total_price)

        tv_header_productname.setText(productname)
        Log.e("iamgepath","::::"+productimage)
        Picasso.with(this).load(productimage).into(tv_header_productimage)
    }

    private fun setDatatoAdapter(data: OrderData?) {
        val dAdapter = OrederInfoAdapter(this@OrderDetailsActivity, data)
        val mLayoutManager = LinearLayoutManager(this)
        rv_OrderDetails.setLayoutManager(mLayoutManager)
        rv_OrderDetails.setItemAnimator(DefaultItemAnimator())
        rv_OrderDetails.setAdapter(dAdapter)
        dAdapter.notifyDataSetChanged()
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private fun doPrint() {
        // Get a PrintManager instance
        val printManager = this
                .getSystemService(Context.PRINT_SERVICE) as PrintManager

        // Set job name, which will be displayed in the print queue
        val jobName = this.getString(R.string.app_name) + " Document"

        // Start a print job, passing in a PrintDocumentAdapter implementation
        // to handle the generation of a print document
        printManager.print(jobName, MyPrintDocumentAdapter(this),
                null) //
    }



}
