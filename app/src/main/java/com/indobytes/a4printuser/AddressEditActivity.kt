package com.indobytes.a4printuser

import android.app.Dialog
import android.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.Helper.ConnectionManager
import com.indobytes.a4printuser.model.APIResponse.AddAddressesResponse
import com.indobytes.a4printuser.model.APIResponse.AddressesResponseDetails
import com.indobytes.a4printuser.model.APIResponse.UpdateAddressesResponse
import com.indobytes.a4printuser.model.ApiInterface
import kotlinx.android.synthetic.main.app_bar_main.*
import retrofit2.Call
import retrofit2.Callback


class AddressEditActivity : AppCompatActivity(){
    var rv_address_id:RecyclerView?=null
    var tv_new_address:TextView?=null
    var fragmentName: Fragment? = null

    internal lateinit var et_name_id: EditText
    internal lateinit var et_company_id:EditText
    internal lateinit var et_address_one_id:EditText
    internal lateinit var et_address_two_id:EditText
    internal lateinit var et_state_id:EditText
    internal lateinit var et_city_id:EditText
    internal lateinit var et_pincode_id:EditText
    internal lateinit var et_address_type:EditText
    internal lateinit var tv_update_id: TextView
    internal lateinit var name_stg_get: String
    internal lateinit var company_stg_get:String
    internal lateinit var address_1_stg:String
    internal lateinit var address_2_stg:String
    internal lateinit var state_stg_get:String
    internal lateinit var city_stg_get:String
    internal lateinit var pincode_stg_get: String
    internal lateinit var address_id_stg: String
     var is_billing: String ="0"
     var is_shipping: String="0"
     var is_default_billing: String="0"
     var is_default_shipping: String="0"
    internal lateinit var sessionManager: SessionManager
    val addr_type_list = listOf("Billing", "Shipping")
    var dialog_list: Dialog?=null
    var list_items_list: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_address_editable)
        sessionManager = SessionManager(this)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        var rootview = findViewById(R.id.rootview) as LinearLayout
        toolbar.setNavigationOnClickListener {
            val intent = Intent(this@AddressEditActivity,MainActivity::class.java)
            finish()
            startActivity(intent)}
        toolbar.setTitle("UPDATE ADDRESS")
        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)
        list_items_list = dialog_list!!.findViewById(R.id.list_languages)
        myloading()

        et_name_id = findViewById(R.id.et_name_id) as EditText
        et_company_id = findViewById(R.id.et_company_id) as EditText
        et_address_one_id = findViewById(R.id.et_address_one_id) as EditText
        et_address_two_id = findViewById(R.id.et_address_two_id) as EditText
        et_state_id = findViewById(R.id.et_state_id) as EditText
        et_city_id = findViewById(R.id.et_city_id) as EditText
        et_pincode_id = findViewById(R.id.et_pincode_id) as EditText
        tv_update_id = findViewById(R.id.tv_update_id) as TextView
        et_address_type = findViewById(R.id.et_address_type) as EditText

        et_address_type.setOnClickListener(View.OnClickListener {
            if (!dialog_list!!.isShowing())
                dialog_list!!.show()
            val state_array_adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, addr_type_list)
            list_items_list!!.setAdapter(state_array_adapter)
            list_items_list!!.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val addr_type_selected = list_items_list!!.getItemAtPosition(i).toString()
                et_address_type.setText(addr_type_selected)
                if(et_address_type.text.toString().equals("Billing")){
                    is_billing = "1"
                    is_default_billing = "1"
                    is_shipping = "0"
                    is_default_shipping = "0"
                }
                if(et_address_type.text.toString().equals("Shipping")){
                    is_billing = "0"
                    is_default_billing = "0"
                    is_shipping = "1"
                    is_default_shipping = "1"
                }
                dialog_list!!.dismiss()
            })
        })


        if (intent.getStringExtra("address_type").equals("new", ignoreCase = true)) {
            et_address_type.visibility = View.GONE
        } else {
            et_address_type.visibility = View.VISIBLE
                address_id_stg = intent.getStringExtra("address_id")
                val name_stg = intent.getStringExtra("name")
                val company_stg = intent.getStringExtra("company")
                val address_stg = intent.getStringExtra("address")
                val address_two_stg = intent.getStringExtra("address_two")
                val city_stg = intent.getStringExtra("city")
                val state_stg = intent.getStringExtra("state")
                val pincode_stg = intent.getStringExtra("pincode")
                val addr_type_bil = intent.getStringExtra("addr_type_billing")
                val addr_type_ship = intent.getStringExtra("addr_type_shipping")
                if(addr_type_bil.equals("1")){
                    et_address_type.setText("Billing")
                    is_billing = "1"
                    is_default_billing = "1"
                    is_shipping = "0"
                    is_default_shipping = "0"
                }
                if (addr_type_ship.equals("1")){
                    et_address_type.setText("Shipping")
                    is_billing = "0"
                    is_default_billing = "0"
                    is_shipping = "1"
                    is_default_shipping = "1"
                }
                et_name_id.setText(name_stg)
                et_company_id.setText(company_stg)
                et_address_one_id.setText(address_stg)
                et_address_two_id.setText(address_two_stg)
                et_state_id.setText(state_stg)
                et_city_id.setText(city_stg)
                et_pincode_id.setText(pincode_stg)

        }

        tv_update_id.setOnClickListener {
            //updateAddress
            name_stg_get = et_name_id.text.toString()
            company_stg_get = et_company_id.text.toString()
            address_1_stg = et_address_one_id.text.toString()
            address_2_stg = et_address_two_id.text.toString()
            state_stg_get = et_state_id.text.toString()
            city_stg_get = et_city_id.text.toString()
            pincode_stg_get = et_pincode_id.text.toString()
            if (hasValidCredentials()) {
                if(ConnectionManager.checkConnection(applicationContext)) {
                    if (intent.getStringExtra("address_type").equals("new", ignoreCase = true)) {
                        CallAddAddressAPI(sessionManager.isUserId,name_stg_get,company_stg_get,address_1_stg,address_2_stg,city_stg_get,state_stg_get,pincode_stg_get)
                    } else {
                        CallUpdateAddressAPI(address_id_stg,name_stg_get,company_stg_get,address_1_stg,address_2_stg,city_stg_get,state_stg_get,pincode_stg_get)
                    }
                }
                else{
                    ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview,applicationContext)
                }
            }
        }
    }

    private fun CallUpdateAddressAPI(user_id: String, name_stg_get: String, company_stg_get: String, address_1_stg: String, address_2_stg: String, city_stg_get: String, state: String, pincode_stg_get: String) {
        my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.updateAddresses(ApiInterface.ENCRIPTION_KEY,user_id,name_stg_get,company_stg_get,address_1_stg,address_2_stg,city_stg_get,state,pincode_stg_get,is_billing,is_shipping,is_default_shipping, is_default_billing,sessionManager.isUserId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UpdateAddressesResponse> {
            override fun onResponse(call: Call<UpdateAddressesResponse>, response: retrofit2.Response<UpdateAddressesResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    var list: AddressesResponseDetails? = response.body()!!.address_info
                    Log.w("Result_Address_resposne","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1")) {
                        val intent = Intent(this@AddressEditActivity,MyAddressActivity::class.java)
                        finish()
                        startActivity(intent)


                    }

                }
            }
            override fun onFailure(call: Call<UpdateAddressesResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }

    private fun CallAddAddressAPI(user_id: String, name_stg_get: String, company_stg_get: String, address_1_stg: String, address_2_stg: String, city_stg_get: String, state: String, pincode_stg_get: String) {
        my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.addAddresses(ApiInterface.ENCRIPTION_KEY,user_id,name_stg_get,company_stg_get,address_1_stg,address_2_stg,city_stg_get,state,pincode_stg_get,"0","0","0","0")
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<AddAddressesResponse> {
            override fun onResponse(call: Call<AddAddressesResponse>, response: retrofit2.Response<AddAddressesResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    var list: AddressesResponseDetails? = response.body()!!.address_info
                    Log.w("Result_Address_REsponse","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1")) {
                        val intent = Intent(this@AddressEditActivity,MyAddressActivity::class.java)
                        finish()
                        startActivity(intent)

                    }

                }
            }

            override fun onFailure(call: Call<AddAddressesResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }

    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(name_stg_get)) {
            et_name_id.error = "Please Provide Username"
        } else if (TextUtils.isEmpty(company_stg_get)) {
            et_company_id.error = "Please Provide Company Name"
        } else if (TextUtils.isEmpty(address_1_stg)) {
            et_address_one_id.error = "Please Provide Address"
        } else if (pincode_stg_get != null && pincode_stg_get.length < 5) {
            et_pincode_id.error = "Please Enter a Valid Pin Code"
        } else if (TextUtils.isEmpty(address_2_stg)) {
            et_address_two_id.error = "Please Provide Address"
        } else if (TextUtils.isEmpty(state_stg_get)) {
            et_state_id.error = "Please Select State"
        } else if (TextUtils.isEmpty(city_stg_get)) {
            et_city_id.error = "Please Provide City"
        } else
            return true
        return false
    }

    override fun onBackPressed() {
        val intent = Intent(this@AddressEditActivity,MainActivity::class.java)
        finish()
        startActivity(intent)
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
}