package com.indobytes.a4printuser

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.Window
import android.widget.*
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.Helper.ConnectionManager
import com.indobytes.a4printuser.model.APIResponse.RegisterResponse
import com.indobytes.a4printuser.model.APIResponse.UpdateProfileResponse
import com.indobytes.a4printuser.model.ApiInterface
import kotlinx.android.synthetic.main.app_bar_main.*
import retrofit2.Call
import retrofit2.Callback

class RegisterActivity : AppCompatActivity() {
    internal lateinit var et_first_id: EditText
    internal lateinit var et_last_id:EditText
    internal lateinit var et_user_name_id:EditText
    internal lateinit var et_email_id:EditText
    internal lateinit var et_company_id:EditText
    internal lateinit var et_phone_no_id:EditText
    internal lateinit var et_alter_no_id:EditText
    internal lateinit var et_conf_passwd_no_id:EditText
    internal lateinit var et_passwd_no_id:EditText
    internal lateinit var et_create_id:TextView
    internal lateinit var et_ref_id:TextView
    internal lateinit var et_terms_checkbox:CheckBox

    internal lateinit var refer_select_stg:String
    var list_items_list: ListView? = null
    var dialog_list: Dialog?=null
    val referedByArr = listOf("Internet Search", "Magazine", "Vehicle Ad", "Flyer", "Billboard Ad", "Interner Ad", "Printing Tag", "Social Network", "Direct Mail Postcard", "Radio Commercial", "Client Referral", "Sales Rep", "Below Zero Graphics", "TV Comercial")
    internal lateinit var first_name_stg_get: String
    internal lateinit var last_name_stg_get:String
    internal lateinit var company_stg_get:String
    internal lateinit var phone_stg_get:String
    internal lateinit var alter_phone_stg_get:String
    internal lateinit var sessionManager: SessionManager

    internal lateinit var username_stg_get:String
    internal lateinit var emailname_stg_get:String
    internal lateinit var passwd_stg_get:String
    internal lateinit var conf_passwd_stg_get:String
    internal lateinit var alter_referrer_stg_get:String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        sessionManager = SessionManager(this)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setTitle("REGISTER")
        var rootview = findViewById(R.id.rootview) as LinearLayout
        et_first_id = findViewById(R.id.reg_firstname) as EditText
        et_last_id = findViewById(R.id.reg_lastname) as EditText
        et_user_name_id = findViewById(R.id.reg_username) as EditText
        et_email_id = findViewById(R.id.reg_email) as EditText
        et_company_id = findViewById(R.id.reg_company) as EditText
        et_phone_no_id = findViewById(R.id.reg_phone_number) as EditText
        et_alter_no_id = findViewById(R.id.reg_alt_number) as EditText
        et_passwd_no_id = findViewById(R.id.reg_password) as EditText
        et_conf_passwd_no_id = findViewById(R.id.reg_conf_passsword) as EditText
        et_terms_checkbox = findViewById(R.id.terms) as CheckBox
        myloading()
        et_ref_id = findViewById(R.id.reg_referryby) as TextView

        et_create_id = findViewById(R.id.reg_create_account) as TextView

        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)

        list_items_list = dialog_list!!.findViewById(R.id.list_languages)


        et_create_id.setOnClickListener(View.OnClickListener {
            first_name_stg_get = et_first_id.getText().toString().trim({ it <= ' ' })
            last_name_stg_get = et_last_id.getText().toString().trim({ it <= ' ' })

            username_stg_get = et_user_name_id.getText().toString().trim({ it <= ' ' })
            emailname_stg_get = et_email_id.getText().toString().trim({ it <= ' ' })

            company_stg_get = et_company_id.getText().toString().trim({ it <= ' ' })
            phone_stg_get = et_phone_no_id.getText().toString().trim({ it <= ' ' })
            alter_phone_stg_get = et_alter_no_id.getText().toString().trim({ it <= ' ' })

            passwd_stg_get = et_passwd_no_id.getText().toString().trim({ it <= ' ' })
            conf_passwd_stg_get = et_conf_passwd_no_id.getText().toString().trim({ it <= ' ' })
            alter_referrer_stg_get = et_ref_id.getText().toString().trim({ it <= ' ' })


            if (hasValidCredentials()) {
                if(et_terms_checkbox.isChecked){
                    if(ConnectionManager.checkConnection(applicationContext)) {
                        callRegisterAPI(first_name_stg_get,last_name_stg_get,username_stg_get,emailname_stg_get,company_stg_get,phone_stg_get,alter_phone_stg_get,passwd_stg_get,conf_passwd_stg_get,alter_referrer_stg_get)
                    }
                    else{
                        ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview,applicationContext)
                    }
                }
                else{
                    Toast.makeText(applicationContext,"Please select terms and conditions",Toast.LENGTH_SHORT).show()
                }
            }
        })


        et_ref_id.setOnClickListener(View.OnClickListener {


                if (!dialog_list!!.isShowing())
                    dialog_list!!.show()
                val state_array_adapter = ArrayAdapter<String>(this@RegisterActivity, android.R.layout.simple_spinner_item, referedByArr)
                list_items_list!!.setAdapter(state_array_adapter)
                list_items_list!!.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                    refer_select_stg = list_items_list!!.getItemAtPosition(i).toString()
                    et_ref_id.setText(refer_select_stg)
                    dialog_list!!.dismiss()
                })

        })

    }

    private fun callRegisterAPI(first_name_stg_get: String, last_name_stg_get: String, last_username_stg_get: String, last_emailname_stg_get: String, company_stg_get: String, phone_stg_get: String, alter_phon_number: String, alter_passwd_stg_get1: String, alter_conf_passwd_stg_get: String, alter_referrer_stg_get: String) {
        my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.registerAccount(ApiInterface.ENCRIPTION_KEY,first_name_stg_get,last_name_stg_get,last_username_stg_get,last_emailname_stg_get,company_stg_get,phone_stg_get,alter_phon_number,alter_passwd_stg_get1,alter_referrer_stg_get,"1")
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<RegisterResponse> {
            override fun onResponse(call: Call<RegisterResponse>, response: retrofit2.Response<RegisterResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1")) {
                        Toast.makeText(applicationContext,response.body()!!.result,Toast.LENGTH_SHORT).show()
                        val intent = Intent(this@RegisterActivity,LoginActivity::class.java)
                        finish()
                        startActivity(intent)

                    }else if(response.body()!!.status.equals("0")){
                        Toast.makeText(applicationContext,response.body()!!.result,Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }

    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(first_name_stg_get)) {
            et_first_id.setError("Please Provide First Name")
        } else if (TextUtils.isEmpty(last_name_stg_get)) {
            et_last_id.setError("Please Provide Last Name")
        } else if (TextUtils.isEmpty(username_stg_get)) {
            et_user_name_id.setError("Please Provide UserName")
        } else if (TextUtils.isEmpty(emailname_stg_get)) {
            et_email_id.setError("Please provide email")
        }  else if (TextUtils.isEmpty(company_stg_get)) {
            et_company_id.setError("Please Provide Company Name")
        } else if (TextUtils.isEmpty(phone_stg_get)) {
            et_phone_no_id.setError("Please Provide Phone Number")
        } else if (TextUtils.isEmpty(passwd_stg_get)) {
            et_passwd_no_id.setError("Please Provide password")
        } else if (TextUtils.isEmpty(conf_passwd_stg_get)) {
            et_conf_passwd_no_id.setError("Please Provide confirm password")
        } else
            return true
        return false
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

}
