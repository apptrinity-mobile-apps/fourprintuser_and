package com.indobytes.a4printuser

import android.app.Dialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.indobytes.a4printuser.Helper.ConnectionManager
import com.indobytes.a4printuser.model.APIResponse.ChangePasswordResponse
import com.indobytes.a4printuser.model.ApiInterface
import kotlinx.android.synthetic.main.app_bar_main.*
import retrofit2.Call
import retrofit2.Callback

class ForgotPasswordActivity : AppCompatActivity() {
    internal lateinit var et_email_id: EditText
    internal lateinit var tv_recov_pass_id: TextView
    internal lateinit var toolbar_title:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        myloading()
        var rootview = findViewById(R.id.rootview) as RelativeLayout
        toolbar.setNavigationOnClickListener { onBackPressed() }
        val forgot_all = intent.getStringExtra("forgot")
        if (forgot_all.equals("password", ignoreCase = true)) {
            toolbar_title = "FORGOT PASSWORD"
        } else {
            toolbar_title = "FORGOT USERNAME"
        }
        toolbar.setTitle(toolbar_title)
        tv_recov_pass_id = findViewById(R.id.tv_recov_pass_id) as TextView
        et_email_id = findViewById(R.id.et_email_id) as EditText
        //forgotPassword

        tv_recov_pass_id.setOnClickListener(View.OnClickListener {
            Log.e("email_lll", et_email_id.getText().toString())
            if (hasValidCredentials()) {
                if(ConnectionManager.checkConnection(applicationContext)) {
                    if (forgot_all.equals("password", ignoreCase = true)) {
                        callForgotPasswordAPI(et_email_id.getText().toString())
                    } else {
                        callForgotUserNameAPI(et_email_id.getText().toString())
                    }
                }
                else{
                    ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
                }

            }
        })
    }

    private fun callForgotUserNameAPI(email: String) {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.forgotUserName(ApiInterface.ENCRIPTION_KEY,email)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ChangePasswordResponse> {
            override fun onResponse(call: Call<ChangePasswordResponse>, response: retrofit2.Response<ChangePasswordResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1") && response.body()!!.Message!=null) {
                        Toast.makeText(applicationContext, response.body()!!.Message, Toast.LENGTH_SHORT).show()
                        onBackPressed()
                    }else{
                        Toast.makeText(applicationContext, response.body()!!.Message, Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }

    private fun callForgotPasswordAPI(email: String) {
        val apiService = ApiInterface.create()

        val call = apiService.forgotPassword(ApiInterface.ENCRIPTION_KEY,email)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ChangePasswordResponse> {
            override fun onResponse(call: Call<ChangePasswordResponse>, response: retrofit2.Response<ChangePasswordResponse>?) {
                if (response != null) {
                    Log.w("Result_Address","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1") && response.body()!!.Message!=null) {
                        Toast.makeText(applicationContext, response.body()!!.Message, Toast.LENGTH_SHORT).show()
                        onBackPressed()
                    }else{
                        Toast.makeText(applicationContext, response.body()!!.Message, Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })
    }
    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(et_email_id.getText().toString()))
            et_email_id.setError("Email Required")
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(et_email_id.getText().toString()).matches())
            et_email_id.setError("Invalid Email")
        else
            return true
        return false
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
}
