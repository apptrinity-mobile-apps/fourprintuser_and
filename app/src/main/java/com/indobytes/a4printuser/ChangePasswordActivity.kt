package com.indobytes.a4printuser

import android.app.Dialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.Helper.ConnectionManager
import com.indobytes.a4printuser.model.APIResponse.ChangePasswordResponse
import com.indobytes.a4printuser.model.ApiInterface
import kotlinx.android.synthetic.main.app_bar_main.*
import retrofit2.Call
import retrofit2.Callback

class ChangePasswordActivity : AppCompatActivity() {
    internal lateinit var et_pass_old_id: EditText
    internal lateinit var et_pass_new_id:EditText
    internal lateinit var et_pass_conf_id:EditText
    internal var old_pass_stg: String? = null
    internal var new_pass_stg:String? = null
    internal lateinit var conf_pass_stg:String
    internal lateinit var tv_change_pass_id: TextView
    internal lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        var rootview = findViewById(R.id.rootview) as RelativeLayout

        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setTitle("CHANGE PASSWORD")
        myloading()
        sessionManager = SessionManager(this)
        val user = sessionManager.userDetails
        val user_id = user.get(SessionManager.PROFILE_ID)

        et_pass_old_id = findViewById(R.id.et_pass_old_id) as EditText
        et_pass_new_id = findViewById(R.id.et_pass_new_id) as EditText
        et_pass_conf_id = findViewById(R.id.et_pass_conf_id) as EditText

        tv_change_pass_id = findViewById(R.id.tv_change_pass_id) as TextView
        //changePassword
        tv_change_pass_id.setOnClickListener(View.OnClickListener {
            old_pass_stg = et_pass_old_id.getText().toString()
            new_pass_stg = et_pass_new_id.getText().toString()
            conf_pass_stg = et_pass_conf_id.getText().toString()
            if (hasValidCredentials()) {
                if(ConnectionManager.checkConnection(applicationContext)) {
                    callChangePasswordAPI(old_pass_stg,new_pass_stg,conf_pass_stg)
                }
                else{
                    ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)

                }


            }
        })
    }

    private fun callChangePasswordAPI(old_pass_stg: String?, new_pass_stg: String?, conf_pass_stg: String) {
        my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.changePassword(ApiInterface.ENCRIPTION_KEY,sessionManager.isUserId, old_pass_stg!!, new_pass_stg!!)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ChangePasswordResponse> {
            override fun onResponse(call: Call<ChangePasswordResponse>, response: retrofit2.Response<ChangePasswordResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1") && response.body()!!.Message!=null) {
                        Toast.makeText(applicationContext, response.body()!!.Message, Toast.LENGTH_SHORT).show()
                        onBackPressed()
                    }else{
                        Toast.makeText(applicationContext, response.body()!!.Message, Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                Log.w("Response_Product","Result : Failed")
            }
        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(old_pass_stg)) {
            et_pass_old_id.setError("Old Password ")
        } else if (old_pass_stg != null && old_pass_stg!!.length < 6) {
            et_pass_old_id.setError("Password Must Contain atleast 6 Characters")
        } else if (TextUtils.isEmpty(new_pass_stg)) {
            et_pass_new_id.setError("new Password")
        } else if (new_pass_stg != null && new_pass_stg!!.length < 6) {
            et_pass_new_id.setError("Password Must Contain atleast 6 Characters")
        } else if (TextUtils.isEmpty(conf_pass_stg)) {
            et_pass_conf_id.setError("new Password")
        } else if (new_pass_stg != conf_pass_stg) {
            et_pass_conf_id.setError("Passwords Mismatch")
        } else
            return true
        return false
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

}
