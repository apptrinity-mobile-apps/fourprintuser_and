package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class AddAddressesResponse {
    val address_info: AddressesResponseDetails? = null

    val result: String? = null

    val status: String? = null

}
