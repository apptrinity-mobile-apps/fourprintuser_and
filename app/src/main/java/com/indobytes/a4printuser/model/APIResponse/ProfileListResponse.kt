package com.indobytes.a4printuser.model.APIResponse

class ProfileListResponse {
     val result: String? = null

     val status: String? = null

     val user_info: Array<UserAddressInfo>? = null
}
