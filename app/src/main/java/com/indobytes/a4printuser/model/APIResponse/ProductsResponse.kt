package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
class ProductsResponse {
    var status: Int = 0
    var result: String? = null
    var data: ArrayList<ProductsResponseDetails>? = null

}

