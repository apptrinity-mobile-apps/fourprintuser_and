package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UPSObjectwithCoupon {
    val product:Product?=null

    val upsprice:String?=null

    val zipcode:String?=null

    val coupon: Coupon? = null
}
