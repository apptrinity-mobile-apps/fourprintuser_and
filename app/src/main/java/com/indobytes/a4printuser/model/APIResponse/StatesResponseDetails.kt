package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class StatesResponseDetails {
    var id: String? = null
    var name: String? = null
    var abbrev: String? = null
}
