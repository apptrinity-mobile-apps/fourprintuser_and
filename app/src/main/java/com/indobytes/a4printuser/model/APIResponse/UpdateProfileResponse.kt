package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UpdateProfileResponse {


    val user_info: User_info? = null

    val result: String? = null

    val status: String? = null

}
