package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Design_comments {
     val custom_design: String? = null

     val design_instructions: String? = null

     val design_notes: String? = null

     val design_back_text: String? = null

     val design_front_text: String? = null
}
