package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class ProductsResponseDetails {
    var id: String? = null
    var name: String? = null
    var image: String? = null
    var product_description: String? = null
}
