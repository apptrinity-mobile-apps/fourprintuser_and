package com.indobytes.a4printuser.model

import com.indobytes.a4printuser.model.APIResponse.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import retrofit2.http.POST
import retrofit2.http.Multipart
import android.util.Log
import okhttp3.*


/**
 * Created by indo67 on 4/13/2018.
 */
public interface ApiInterface  {


    @GET("/Mobile/products")
    abstract fun getProductDetails(@Header("encryption_key") encrypt_key: String): Call<ProductsResponse>

    @FormUrlEncoded
    @POST("/Mobile/addresses")
    abstract fun getAddresses(@Header("encryption_key") encrypt_key: String,
                              @Field("user_id") user_Id: String): Call<AddressesResponse>

    @FormUrlEncoded
    @POST("/Mobile/addAddress")
    abstract fun addAddresses(@Header("encryption_key") encrypt_key: String,
                              @Field("user_id") user_id: String,
                              @Field("name") name: String,
                              @Field("company_name") company_name: String,
                              @Field("address1") address1: String,
                              @Field("address2") address2: String,
                              @Field("city") city: String,
                              @Field("state") state: String,
                              @Field("zipcode") zipcode: String,
                              @Field("is_billing") is_billing: String,
                              @Field("is_shipping") is_shipping: String,
                              @Field("is_default_shipping") is_default_shipping: String,
                              @Field("is_default_billing") is_default_billing: String): Call<AddAddressesResponse>

    @FormUrlEncoded
    @POST("/Mobile/updateAddress")
    abstract fun updateAddresses(@Header("encryption_key") encrypt_key: String,
                                 @Field("address_id") address_id: String,
                                 @Field("name") name: String,
                                 @Field("company_name") company_name: String,
                                 @Field("address1") address1: String,
                                 @Field("address2") address2: String,
                                 @Field("city") city: String,
                                 @Field("state") state: String,
                                 @Field("zipcode") zipcode: String,
                                 @Field("is_billing") is_billing: String,
                                 @Field("is_shipping") is_shipping: String,
                                 @Field("is_default_shipping") is_default_shipping: String,
                                 @Field("is_default_billing") is_default_billing: String,
                                 @Field("user_id") user_id: String): Call<UpdateAddressesResponse>


    @FormUrlEncoded
    @POST("/Mobile/deleteAddress")
    abstract fun deleteAddresses(@Header("encryption_key") encrypt_key: String,
                                 @Field("address_id") address_id: String,
                                 @Field("name") name: String,
                                 @Field("company_name") company_name: String,
                                 @Field("address1") address1: String,
                                 @Field("address2") address2: String,
                                 @Field("city") city: String,
                                 @Field("state") state: String,
                                 @Field("zipcode") zipcode: String): Call<DeleteAddressesResponse>


    @FormUrlEncoded
    @POST("/Mobile/updateProfile")
    abstract fun updateProfile(@Header("encryption_key") encrypt_key: String,
                               @Field("user_id") address_id: String,
                               @Field("first_name") name: String,
                               @Field("last_name") company_name: String,
                               @Field("company") address1: String,
                               @Field("phone_number") address2: String,
                               @Field("alternate_number") city: String,
                               @Field("customer_service_representative") state: String,
                               @Field("referred_by") zipcode: String): Call<UpdateProfileResponse>

    @FormUrlEncoded
    @POST("/Mobile/userProfile")
    abstract fun getProfile(@Header("encryption_key") encrypt_key: String,
                            @Field("user_id") user_id: String): Call<UpdateProfileResponse>


    @FormUrlEncoded
    @POST("/Mobile/login")
    abstract fun login(@Header("encryption_key") encrypt_key: String,
                       @Field("username") username: String,
                       @Field("password") password: String): Call<UserLoginResponse>

    @FormUrlEncoded
    @POST("/Mobile/changePassword")
    abstract fun changePassword(@Header("encryption_key") encrypt_key: String,
                                @Field("user_id") user_id: String,
                                @Field("old_password") old_password: String,
                                @Field("new_password") new_password: String): Call<ChangePasswordResponse>

    @FormUrlEncoded
    @POST("/Mobile/forgotPassword")
    abstract fun forgotPassword(@Header("encryption_key") encrypt_key: String,
                                @Field("email") email: String): Call<ChangePasswordResponse>


    @FormUrlEncoded
    @POST("/Mobile/forgotUser")
    abstract fun forgotUserName(@Header("encryption_key") encrypt_key: String,
                                @Field("email") email: String): Call<ChangePasswordResponse>

    @FormUrlEncoded
    @POST("/Mobile/orders")
    abstract fun getUserOrders(@Header("encryption_key") encrypt_key: String,
                               @Field("user_id") user_id: String,
                               @Field("status") status: String): Call<UserOrderResponse>

    @FormUrlEncoded
    @POST("/Mobile/signup")
    abstract fun registerAccount(@Header("encryption_key") encrypt_key: String,
                                 @Field("first_name") name: String,
                                 @Field("last_name") company_name: String,
                                 @Field("username") user_name: String,
                                 @Field("email") email: String,
                                 @Field("company_name") address1: String,
                                 @Field("phone_number") address2: String,
                                 @Field("alternate_number") city: String,
                                 @Field("password") passwd: String,
                                 @Field("referred_by") zipcode: String,
                                 @Field("terms_conditions") terms: String): Call<RegisterResponse>

    @FormUrlEncoded
    @POST("/Mobile/orderView")
    abstract fun getOrderDetails(@Header("encryption_key") encrypt_key: String,
                                 @Field("order_id") order_id: String): Call<OrderDetailsResponse>

    @GET("/Mobile/states")
    abstract fun getStates(@Header("encryption_key") encrypt_key: String): Call<StatesResponse>


    @FormUrlEncoded
    @POST("/Mobile/getProfileList")
    abstract fun getProfileList(@Header("encryption_key") encrypt_key: String,
                                @Field("user_id") user_id: String,
                                @Field("address_type") address_type: String): Call<ProfileListResponse>

    /*@Multipart
    @POST("/Mobile/fileUpload")
    fun fileUpload(@Header("encryption_key") encrypt_key: String, @Part(MultipartBody) Mufile_name filePart): Call<FileUploadResponse>*/

    @Multipart
    @POST("/Mobile/fileUpload")
    fun fileUpload(
            @Header("encryption_key") encrypt_key: String,
            @Part file_name: MultipartBody.Part): Call<FileUploadResponse>

    //abstract fun fileUpload(@Header("encryption_key") encrypt_key: String,@Part file: MultipartBody.Part): Call<FileUploadResponse>


    @FormUrlEncoded
    @POST("/Mobile/getProfile")
    abstract fun getMyProfile(@Header("encryption_key") encrypt_key: String,
                              @Field("user_id") user_id: String,
                              @Field("address_id") address_id: String): Call<NewProfileResponse>

    @FormUrlEncoded
    @POST("/Mobile/getDefaultProfile")
    abstract fun getMyDefaultProfile(@Header("encryption_key") encrypt_key: String,
                              @Field("user_id") user_id: String,
                              @Field("address_type") address_type: String): Call<NewProfileResponse>


    @FormUrlEncoded
    @POST("/Mobile/getUPSRates")
    abstract fun getUPSRates(@Header("encryption_key") encrypt_key: String,
                                     @Field("zipcode") zipcode: String,
                                     @Field("PAGES") PAGES: String,
                                     @Field("PAPER OR STOCK")  PAPER_OR_STOCK: String,
                                     @Field("PRINT SIZE")  PRINT_SIZE: String,
                                     @Field("paper_type")  paper_type: String,
                                     @Field("Quantity")  Quantity: String,
                                     @Field("COVER PAPER TYPE ")  COVER_PAPER_TYPE : String): Call<UPSResponse>


    @POST("/Mobile/saveOrder")
    fun saveOrder(@Header("encryption_key") encrypt_key: String,
                  @Body hashMap: HashMap<String, Any>): Call<UPSResponse>


    companion object Factory {
        val BASE_URL = "https://www.4print.com"
        const val IMAGE_BASE_URL = "https://www.4print.com/uploads/products/"
        val ENCRIPTION_KEY = "7d94c049db8081c50400d84cb96ed302"
        const val IMAGE_BASE_ORDER_URL = "https://www.4print.com/uploads/orders/"


        var okHttpClient = OkHttpClient.Builder()
                .addInterceptor(object : Interceptor {
                    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                        val request = chain.request()
                        val response = chain.proceed(request)

                        // todo deal with the issues the way you need to
                        if (response.code() == 500) {
                            Log.i("Retrofit:Info ","Retrofit_Response:: "+response.code() )
                            return response
                        }

                        return response
                    }
                })
                .build()

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(ApiInterface::class.java)
        }



    }

}
