package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Coupon {
     val crazy_fast_price: String? = null

     val fast_price: String? = null

     val faster_price: String? = null

     val economy_price: String? = null

     val offercode: String? = null
}
