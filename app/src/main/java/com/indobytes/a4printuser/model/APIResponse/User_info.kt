package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties


@JsonIgnoreProperties(ignoreUnknown = true)
class User_info {

    val phone_number: String? = null
    val signup_newsletter: String? = null
    val status: String? = null
    val terms_conditions: String? = null
    val alternate_number: String? = null
    var customer_service_representative: String? = null
    val password: String? = null
    var referred_by: String? = null
    val id: String? = null
    val first_name: String? = null
    val username: String? = null
    val create_date: String? = null
    val email: String? = null
    val company: String? = null
    val last_name: String? = null

}
