package com.indobytes.a4printuser.model.APIResponse

class ProductDesign {
     val custom_design: String? = null

     val design_instructions: String? = null

     val design_notes: String? = null

     val design_back_text: String? = null

     val design_front_text: String? = null
}
