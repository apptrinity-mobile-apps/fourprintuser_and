package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class AddressesResponse {
     val result: String? = null

     val status: String? = null

     val data: Array<AddressesResponseDetails>? = null
}
