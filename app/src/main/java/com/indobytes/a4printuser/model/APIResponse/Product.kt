package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Product {

      val product_meta: Product_meta? = null

    //  val ExtraCharges: Array<ExtraCharges>? = null

      val productid: String? = null

      //val sublabels: Sublabels? = null

     // val AddOptionals: Array<String>? = null

      val Price: String? = null

      val Price_type: String? = null

      val pricingid: String? = null

      val paper_type: String? = null

      val productname: String? = null


}
