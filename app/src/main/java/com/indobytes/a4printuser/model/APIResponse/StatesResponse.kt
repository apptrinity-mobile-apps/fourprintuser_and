package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class StatesResponse {
    var status: Int = 0
    var result: String? = null
    var data: ArrayList<StatesResponseDetails>? = null

}

