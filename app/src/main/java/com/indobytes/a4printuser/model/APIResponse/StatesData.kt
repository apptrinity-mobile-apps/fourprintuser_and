package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class StatesData {
     val id: String? = null

     val name: String? = null

     val abbrev: String? = null
}
