package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.annotation.JsonInclude
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@JsonIgnoreProperties(ignoreUnknown = true)
class UPSResponseInfo {

    @SerializedName("2nd Day Air")
    @Expose
     val  UPS_2nd_Day_Air:String = ""

    @SerializedName("UPS 3 Day Select")
    @Expose
    val  UPS_3_Day_Select:String = ""

    @SerializedName("UPS Ground")
    @Expose
    val  UPS_Ground:String=""

    @SerializedName("UPS Next Day Air")
    @Expose
    val  UPS_Next_Day_Air:String=""

    @SerializedName("UPS Next Day Air Saver")
    @Expose
    val  UPS_Next_Day_Air_Saver:String=""

    @SerializedName("UPS Next Day Air Early")
    @Expose
    val  UPS_Next_Day_Air_Early:String=""

    @SerializedName("UPS 2nd Day Air (Saturday Delivery)")
    @Expose
    val  UPS_2nd_Day_Air_Saturday_Delivery:String=""

    @SerializedName("UPS 2nd Day Air A.M.")
    @Expose
    val  UPS_2nd_DayAir :String=""
}
