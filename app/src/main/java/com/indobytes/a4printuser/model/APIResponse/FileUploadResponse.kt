package com.indobytes.a4printuser.model.APIResponse

class FileUploadResponse {
    val result: String? = null

    val status: String? = null

   // val data: String? = null

    val data: UploadDataResponse? = null
}
