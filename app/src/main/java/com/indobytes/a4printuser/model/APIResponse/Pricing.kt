package com.indobytes.a4printuser.model.APIResponse

class Pricing {
     val shopping_price: String? = null

     val total_price: String? = null

     val print_price: String? = null
}
