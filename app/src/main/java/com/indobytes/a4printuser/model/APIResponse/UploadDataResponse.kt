package com.indobytes.a4printuser.model.APIResponse
class UploadDataResponse{
     val response_filename: String? = null

     val upload_filename: String? = null

     val size: String? = null
}