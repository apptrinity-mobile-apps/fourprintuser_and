package com.indobytes.a4printuser.model.APIResponse

class Shipping {
     val blind_shipping: String? = null

     val alternate_email: String? = null

     val address: String? = null

     val company: String? = null

     val name: String? = null

     val delivery_method: String? = null

     val address2: String? = null

     val rate_estimate_id: String? = null

     val address_type: String? = null
}
