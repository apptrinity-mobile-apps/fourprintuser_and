package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UserOrderInfo {
    val order_id: String? = null

    val product_id: String? = null

    val created_on: String? = null

    val product_name: String? = null

    val order_status: String? = null

    val image:String?=null
}
