package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Product_meta {

    val  BINDING: String? = null

    val  Quantity: String? = null

    val  PAPER_OR_STOCK: String? = null

    val  EXACT_SIZE: String? = null

    val  Exact_Quantity: String? = null

    val  PAGES: String? = null

    val  Price_Type: String? = null

    val  PRINT_SIZE: String? = null

    val  COVER_PAPER_TYPE: String? = null

    val COLOR_CRITICAL: String? = null

    val  SIDES: String? = null

    val  COATING: String? = null

}
