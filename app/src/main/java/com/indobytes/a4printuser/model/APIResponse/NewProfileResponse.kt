package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class NewProfileResponse {
    val result: String? = null

    val status: String? = null

    val user_info:UserAddressInfo?  = null

}
