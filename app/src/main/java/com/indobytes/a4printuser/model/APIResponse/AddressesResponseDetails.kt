package com.indobytes.a4printuser.model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class AddressesResponseDetails {

     val is_billing: String? = null
     val shipping_instructions: String? = null
     val status: String? = null
     val created_on: String? = null
     val state: String? = null
     val address1: String? = null
     val address2: String? = null
     val shiping_address_type: String? = null
     val city: String? = null
     val pincode: String? = null
     val is_shipping: String? = null
     val address_id: String? = null
     val company_name: String? = null
     val name: String? = null
     val updated_on: String? = null
     val is_default_billing: String? = null
     val is_default_shipping: String? = null
     val user_id: String? = null
}