package com.indobytes.a4printuser.model.APIResponse

class OrderDetailsResponse {
     val result: String? = null

     val status: String? = null

     val data: OrderData? = null
}
