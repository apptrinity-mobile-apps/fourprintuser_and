package com.indobytes.a4printuser.model.APIResponse

class Orderinfo {
     val order_number: String? = null

     val order_placed: String? = null

     val amount_type: String? = null

     val product_name: String? = null

     val delivery_method: String? = null

     val order_status: String? = null
}
