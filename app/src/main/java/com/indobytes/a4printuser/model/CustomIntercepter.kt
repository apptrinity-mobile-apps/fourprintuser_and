package com.indobytes.a4printuser.model

import okhttp3.Interceptor
import okhttp3.Response

class CustomIntercepter : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        val request = chain!!.request()///
        val response = chain!!.proceed(request)


        // for request size

        val requestLength = request.body()!!.contentLength()

        // for response size
        val responseLength = response.body()!!.contentLength()


        return response
    }

}