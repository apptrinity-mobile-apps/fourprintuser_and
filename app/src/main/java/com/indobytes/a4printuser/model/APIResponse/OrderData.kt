package com.indobytes.a4printuser.model.APIResponse

class OrderData {
     val pricing: Pricing? = null

     val billing: Billing? = null

     val shipping: Shipping? = null

     val productMeta: Array<ProductMeta>? = null

     val productDesign: ProductDesign? = null

     val orderinfo: Orderinfo? = null
}
