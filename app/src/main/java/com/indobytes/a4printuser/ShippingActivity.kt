package com.indobytes.a4printuser

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import android.widget.RadioGroup
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.Helper.BundleDataInfo
import com.indobytes.a4printuser.Helper.ConnectionManager
import com.indobytes.a4printuser.Helper.CustomTextView
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import com.indobytes.a4printuser.model.APIResponse.*
import com.indobytes.a4printuser.model.ApiInterface
import kotlinx.android.synthetic.main.activity_review.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback





class ShippingActivity : AppCompatActivity(), View.OnClickListener {


    private lateinit var ll_billinginfor: LinearLayout
    private lateinit var ll_billinginfor_detailview: LinearLayout
    private lateinit var tv_billing_selectprofile: CustomTextView
    private lateinit var et_billing_name: EditText
    private lateinit var et_billing_address: EditText
    private lateinit var et_billing_address_2: EditText
    private lateinit var et_billinginforcity: EditText
    private lateinit var tv_billinginforstate: CustomTextView
    private lateinit var tv_billinginforzipcode: EditText
    private lateinit var chk_billing_savenewprofile: CheckBox
    private lateinit var chk_billing_setprofiledefault: CheckBox

    private lateinit var ll_shippingaddress: LinearLayout
    private lateinit var ll_shippingaddr_detailview: LinearLayout
    private lateinit var tv_shippingaddr_selectprofile: CustomTextViewBold
    private lateinit var tv_shippingaddr_addresstype: CustomTextViewBold
    private lateinit var et_shippingaddr_companyname: EditText
    private lateinit var et_shippingaddr_name: EditText
    private lateinit var et_shippingaddr_address: EditText
    private lateinit var et_shippingaddr_address_2: EditText
    private lateinit var et_shippingaddrcity: EditText
    private lateinit var tv_shippingaddrstate: CustomTextView
    private lateinit var et_shippingaddrzipcode: EditText
    private lateinit var chk_shipping_savenewprofile: CheckBox
    private lateinit var chk_shipping_setprofiledefault: CheckBox
    private lateinit var ll_shipmentinfor: LinearLayout
    private lateinit var ll_shipmentinfor_detailview: LinearLayout
    private lateinit var et_shipmentinfor_email: EditText
    private lateinit var et_shipmentinfor_customerrefer: EditText
    private lateinit var chk_shipmentinfor_blindshipping: CheckBox
    private lateinit var ll_shipmentmethod: LinearLayout
    private lateinit var ll_selectshipmethod_detailview: LinearLayout
    private lateinit var tv_shippingmethod: CustomTextView
    private lateinit var btn_revieworder: Button

    private lateinit var radioGroup: RadioGroup

    private lateinit var dialog_list: Dialog
    private lateinit var list_items_list: ListView


    private lateinit var stringStateArrayList: ArrayList<String>
    private lateinit var state_code_array: ArrayList<String>
    internal var stringProfileArrayList = arrayOf("New Profile", "Default Profile")
    internal var stringAddressTypeArrayList = arrayOf("Residential", "Commercial")
    internal var stringShippingMethodArrayList = arrayOf("Ups", "Pickup,Skokie,illinois(Free)")
    private lateinit var stringaddressArrayList: ArrayList<String>
    private lateinit var stringaddressIdBillingList: ArrayList<String>
    private lateinit var stringaddressIdShippingList: ArrayList<String>
    private lateinit var stringaddressArrayList2: ArrayList<String>
    var selected_state_name: String? = null
    var selected_profile_name: String? = null
    lateinit var selected_state_id: String
    lateinit var selected_profile_id: String
    var billing_name: String = ""
    var billing_address: String = ""
    var billing_address_2: String = ""
    var billing_city: String = ""
    var billing_state: String = ""
    var billing_zipcode: String = ""

    var shipping_company_name: String = ""
    var shipping_name: String = ""
    var shipping_address: String = ""
    var shipping_address_2: String = ""
    var shipping_city: String = ""
    var shipping_state: String = ""
    var shipping_zipcode: String = ""

    var shipment_email: String = ""
    var shipment_reference: String = ""

    var shipping_method: String = ""


    var ups_zipcode: String = ""
    var upsprice: String = ""
    var ups_pages: String = ""
    var ups_paperstock: String = ""
    var ups_printsize: String = ""
    var ups_papertype: String = ""
    var ups_quantity: String = ""
    var ups_covertype: String = ""


    var ups_price_shipping: String = "0.00"
    var ups_delevrymethod: String = ""

    var mUPSProductList: ArrayList<Product> = ArrayList<Product>()
    var mUPSProductMetaList: ArrayList<Product_meta> = ArrayList<Product_meta>()

    private lateinit var iv_billinginfor: ImageView
    private lateinit var iv_shippingaddress: ImageView
    private lateinit var iv_shipmentinfor: ImageView
    private lateinit var iv_selectshiipmethod: ImageView


    internal lateinit var ll_order: LinearLayout
    internal lateinit var ll_upload: LinearLayout
    internal lateinit var ll_shipping: LinearLayout
    internal lateinit var ll_review: LinearLayout
    internal lateinit var sessionManager: SessionManager
    var address_id_billing:String?=""
    var address_id_position:Int?=0
    internal lateinit var upsobj:UPSObject
    internal lateinit var rootview:RelativeLayout

    public var mUPSList: ArrayList<String> = ArrayList<String>()

    public var mAddressList: ArrayList<UserAddressInfo> = ArrayList<UserAddressInfo>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shipping)

        sessionManager = SessionManager(this)

        readBundleData()


        //toolbar
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setTitle("SHIPPING")
        rootview = findViewById(R.id.rootview) as RelativeLayout
        ll_order = findViewById(R.id.ll_order) as LinearLayout
        ll_upload = findViewById(R.id.ll_upload) as LinearLayout
        ll_shipping = findViewById(R.id.ll_shipping) as LinearLayout
        ll_review = findViewById(R.id.ll_review) as LinearLayout

        ll_upload.setOnClickListener(this)
        ll_shipping.setOnClickListener(this)
        ll_review.setOnClickListener(this)
        ll_order.setOnClickListener(this)

        //layoout initilaizationi
        initialise()
        myloading()
        //dialog
        dialog_list = Dialog(this)
        dialog_list.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list.setContentView(R.layout.dialog_view)
        dialog_list.setCancelable(true)
        list_items_list = dialog_list.findViewById(R.id.list_languages) as ListView

        getStates()
        getProfileListBilling()
        getProfileListShipping()

        tv_billing_selectprofile.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()

            val profile_array_adapter = ArrayAdapter<String>(this@ShippingActivity, android.R.layout.simple_spinner_item, stringaddressArrayList)
            list_items_list.adapter = profile_array_adapter
            list_items_list.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                selected_profile_name = list_items_list.getItemAtPosition(i).toString()
                tv_billing_selectprofile.setText(selected_profile_name)
                dialog_list.dismiss()
                //selected_profile_id = state_code_array.get(stringaddressArrayList.indexOf(selected_profile_name!!))
                if (selected_profile_name.equals("New Profile")) {
                    et_billing_name.setText("")
                    et_billing_address.setText("")
                    et_billing_address_2.setText("")
                    et_billinginforcity.setText("")
                    tv_billinginforzipcode.setText("")
                    tv_billinginforstate.setText("")
                }else if(selected_profile_name.equals("Default")){

                    if(ConnectionManager.checkConnection(applicationContext)) {
                        getMyDefaultProfile("Billing",true)
                    }
                    else{
                        ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
                    }
                }else{

                    if(ConnectionManager.checkConnection(applicationContext)) {
                        getMyProfile()
                    }
                    else{
                        ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
                    }

                } /*else if (selected_profile_name.equals(state_code_array.get(stringaddressArrayList.indexOf(selected_profile_name!!)))) {




                }*/


                //Log.e("selected_name_id_name", selected_profile_name)

            }
        }

        tv_billinginforstate.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter<String>(this@ShippingActivity, android.R.layout.simple_spinner_item, stringStateArrayList)
            list_items_list.adapter = state_array_adapter
            list_items_list.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                selected_state_name = list_items_list.getItemAtPosition(i).toString()
                tv_billinginforstate.setText(selected_state_name)
                dialog_list.dismiss()
                //selected_state_id = state_code_array.get(stringStateArrayList.indexOf(selected_state_name!!))
                //Log.e("selected_name_id_name", selected_state_id + "\n" + selected_state_name)

            }

        }


        tv_shippingaddr_selectprofile.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val profile_array_adapter = ArrayAdapter<String>(this@ShippingActivity, android.R.layout.simple_spinner_item, stringaddressArrayList2)
            list_items_list.adapter = profile_array_adapter
            list_items_list.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                selected_profile_name = list_items_list.getItemAtPosition(i).toString()
                tv_shippingaddr_selectprofile.setText(selected_profile_name)
                dialog_list.dismiss()
                // selected_profile_id = state_code_array.get(stringProfileArrayList.indexOf(selected_profile_name!!))
                Log.e("selected_name_id_name", selected_profile_name)
                if (selected_profile_name.equals("New Profile")) {

                    et_shippingaddr_companyname.setText("")
                    et_shippingaddr_address.setText("")
                    et_shippingaddr_address_2.setText("")
                    et_shippingaddr_name.setText("")
                    et_shippingaddrcity.setText("")
                    et_shippingaddrzipcode.setText("")
                    tv_shippingaddr_addresstype.setHint("Select Address Type")
                    tv_shippingaddrstate.setText("")

                }else if(selected_profile_name.equals("Default")){

                    if(ConnectionManager.checkConnection(applicationContext)) {
                        getMyDefaultProfile("Shipping",false)
                    }
                    else{
                        ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
                    }
                }else if(selected_profile_name.equals("Same As Billing")){

                    /*if(ConnectionManager.checkConnection(applicationContext)) {
                        getMyDefaultProfile("Billing",false)
                    }
                    else{
                        ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
                    }*/

                        et_shippingaddr_name.setText(et_billing_name.text.toString())
                        et_shippingaddr_companyname.setText("")
                        et_shippingaddr_address.setText(et_billing_address.text.toString())
                        et_shippingaddr_address_2.setText(et_billing_address_2.text.toString())
                        tv_shippingaddr_addresstype.setText("")
                        et_shippingaddrcity.setText(et_billinginforcity.text.toString())
                        tv_shippingaddrstate.setText(tv_billinginforstate.text.toString())
                        et_shippingaddrzipcode.setText(tv_billinginforzipcode.text.toString())
                }else{
                    getMyProfile_Shipping()
                }

            }
        }

        tv_shippingaddr_addresstype.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val profile_array_adapter = ArrayAdapter<String>(this@ShippingActivity, android.R.layout.simple_spinner_item, stringAddressTypeArrayList)
            list_items_list.adapter = profile_array_adapter
            list_items_list.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                selected_profile_name = list_items_list.getItemAtPosition(i).toString()
                tv_shippingaddr_addresstype.setText(selected_profile_name)
                dialog_list.dismiss()
                // selected_profile_id = state_code_array.get(stringProfileArrayList.indexOf(selected_profile_name!!))
                //Log.e("selected_name_id_name", selected_profile_name)


            }
        }

        tv_shippingaddrstate.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val state_array_adapter = ArrayAdapter<String>(this@ShippingActivity, android.R.layout.simple_spinner_item, stringStateArrayList)
            list_items_list.adapter = state_array_adapter
            list_items_list.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                selected_state_name = list_items_list.getItemAtPosition(i).toString()
                tv_shippingaddrstate.setText(selected_state_name)
                dialog_list.dismiss()
                //selected_state_id = state_code_array.get(stringStateArrayList.indexOf(selected_state_name!!))
                //Log.e("selected_name_id_name", selected_state_id + "\n" + selected_state_name)

            }

        }


        ll_billinginfor.setOnClickListener {
            ll_billinginfor_detailview.visibility = View.VISIBLE
            ll_shippingaddr_detailview.visibility = View.GONE
            ll_shipmentinfor_detailview.visibility = View.GONE
            ll_selectshipmethod_detailview.visibility = View.GONE

            iv_billinginfor.setImageDrawable(resources.getDrawable(R.drawable.up_arrow_2))
        }


        ll_shippingaddress.setOnClickListener {

            billing_name = et_billing_name.text.toString()
            billing_address = et_billing_address.text.toString()
            billing_address_2 = et_billing_address_2.text.toString()
            billing_city = et_billinginforcity.text.toString()
            billing_state = tv_billinginforstate.text.toString()
            Log.e("BIllling_STATE", billing_state)
            billing_zipcode = tv_billinginforzipcode.text.toString()

            if (hasValidCredentialsBilling()) {
                ll_billinginfor_detailview.visibility = View.GONE
                ll_shippingaddr_detailview.visibility = View.VISIBLE
                ll_shipmentinfor_detailview.visibility = View.GONE
                ll_selectshipmethod_detailview.visibility = View.GONE

                iv_billinginfor.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))
                iv_shippingaddress.setImageDrawable(resources.getDrawable(R.drawable.up_arrow_2))
                iv_shipmentinfor.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))
                iv_selectshiipmethod.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))
            }

        }
        ll_shipmentinfor.setOnClickListener {

            shipping_company_name = et_shippingaddr_companyname.text.toString()
            shipping_name = et_shippingaddr_name.text.toString()
            shipping_address = et_shippingaddr_address.text.toString()
            shipping_address_2 = et_shippingaddr_address_2.text.toString()
            shipping_city = et_shippingaddrcity.text.toString()
            shipping_state = tv_shippingaddrstate.text.toString()
            shipping_zipcode = et_shippingaddrzipcode.text.toString()

            if (hasValidCredentialsShipping()) {
                ll_billinginfor_detailview.visibility = View.GONE
                ll_shippingaddr_detailview.visibility = View.GONE
                ll_shipmentinfor_detailview.visibility = View.VISIBLE
                ll_selectshipmethod_detailview.visibility = View.GONE

                iv_billinginfor.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))
                iv_shippingaddress.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))
                iv_shipmentinfor.setImageDrawable(resources.getDrawable(R.drawable.up_arrow_2))
                iv_selectshiipmethod.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))

            }
        }
        ll_shipmentmethod.setOnClickListener {

            shipment_email = et_shipmentinfor_email.text.toString()
            shipment_reference = et_shipmentinfor_customerrefer.text.toString()

            if (/*hasValidCredentialsShipment()*/true) {
                ll_billinginfor_detailview.visibility = View.GONE
                ll_shippingaddr_detailview.visibility = View.GONE
                ll_shipmentinfor_detailview.visibility = View.GONE
                ll_selectshipmethod_detailview.visibility = View.VISIBLE

                iv_billinginfor.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))
                iv_shippingaddress.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))
                iv_shipmentinfor.setImageDrawable(resources.getDrawable(R.drawable.down_arrow_2))
                iv_selectshiipmethod.setImageDrawable(resources.getDrawable(R.drawable.up_arrow_2))
            }
        }

        tv_shippingmethod.setOnClickListener {
            if (!dialog_list.isShowing)
                dialog_list.show()
            val profile_array_adapter = ArrayAdapter<String>(this@ShippingActivity, android.R.layout.simple_spinner_item, stringShippingMethodArrayList)
            list_items_list.adapter = profile_array_adapter
            list_items_list.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                selected_profile_name = list_items_list.getItemAtPosition(i).toString()
                tv_shippingmethod.setText(selected_profile_name)
                dialog_list.dismiss()
                // selected_profile_id = state_code_array.get(stringProfileArrayList.indexOf(selected_profile_name!!))
                Log.e("selected_name_id_name", selected_profile_name)
                if(selected_profile_name.equals("Ups")){
                    radioGroup.visibility=View.VISIBLE
                    try {
                        if(ConnectionManager.checkConnection(applicationContext)) {
                            getUPSRating()
                        }
                        else{
                            ConnectionManager.snackBarNetworkAlert(rootview,applicationContext)
                        }

                    }catch (e:Exception){e.printStackTrace()
                    Log.w("exception","LLL "+e.toString())}
                }else{
                    radioGroup.visibility=View.GONE
                    ups_price_shipping="0.00"
                }


            }
        }


        btn_revieworder.setOnClickListener {

            shipping_method = tv_shippingmethod.text.toString()

            if (hasValidCredentialsBilling()) {
                if (hasValidCredentialsShipping()) {
                    if (/*hasValidCredentialsShipment()*/true) {
                        if (hasValidUPS()) {
                            val intent = Intent(this, ReviewActivity::class.java)
                            putBundleData(intent)
                            this.startActivity(intent)
                        }
                    }
                }
            }

        }


    }
    var upload_notes: String = ""
    var upload_instruction: String = ""
    var upload_front: String = ""
    var upload_back: String = ""
    private fun readBundleData() {
        if(intent != null ) {
            upload_notes = intent.getStringExtra(BundleDataInfo.DESIGN_NOTES)
            upload_instruction = intent.getStringExtra(BundleDataInfo.DESIGN_INSTRUCTION)
            upload_front = intent.getStringExtra(BundleDataInfo.DESIGN_FRONT)
            upload_back = intent.getStringExtra(BundleDataInfo.DESIGN_BACK)
        }
    }

    private fun putBundleData(intent: Intent) {
        intent.putExtra(BundleDataInfo.BILLING_INFO_NAME, billing_name)
        intent.putExtra(BundleDataInfo.BILLING_INFO_ADDRESS, billing_address)
        intent.putExtra(BundleDataInfo.BILLING_INFO_ADDRESS2, billing_address_2)
        intent.putExtra(BundleDataInfo.BILLING_INFO_CITY, billing_city)
        intent.putExtra(BundleDataInfo.BILLING_INFO_STATE, billing_state)
        intent.putExtra(BundleDataInfo.BILLING_INFO_ZIPCODE, billing_zipcode)
        intent.putExtra(BundleDataInfo.BILLING_INFO_SELECT_PROFILE, selected_profile_name)
        intent.putExtra(BundleDataInfo.BILLING_INFO_SAVE_NEW_PROFILE, "" + chk_billing_savenewprofile.isChecked)
        intent.putExtra(BundleDataInfo.BILLING_INFO_SAVE_PROFILEAS_DEFAULT, "" + chk_billing_setprofiledefault.isChecked)


        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_NAME, shipping_name)
        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_COMPANY_NAME, shipping_company_name)
        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_ADDRESS, shipping_address)
        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_ADDRESS2, shipping_address_2)
        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_CITY, shipping_city)
        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_STATE, shipping_state)
        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_ZIPCODE, shipping_zipcode)
        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_TYPE, tv_shippingaddr_addresstype.text.toString())
        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_SELECT_PROFILE, selected_profile_name)
        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_SAVE_NEW_PROFILE, "" + chk_shipping_savenewprofile.isChecked)
        intent.putExtra(BundleDataInfo.SHIPPING_ADDRESS_SAVE_PROFILEAS_DEFAULT, "" + chk_shipping_setprofiledefault.isChecked)


        intent.putExtra(BundleDataInfo.SHIPPING_INFO_EMAIL, shipment_email)
        intent.putExtra(BundleDataInfo.SHIPPING_INFO_CC_REFER, shipment_reference)
        intent.putExtra(BundleDataInfo.SHIPPING_INFO_BLIND_SHIPPING, "" + chk_shipmentinfor_blindshipping.isChecked)


        intent.putExtra(BundleDataInfo.SHIPPING_METHOD, shipping_method)
        intent.putExtra(BundleDataInfo.SHIPPING_METHOD_ITEM_PRICE,ups_price_shipping)
        intent.putExtra(BundleDataInfo.SHIPPING_METHOD_ITEM_DELIVERYMETHOD,ups_delevrymethod)

        //DESIGN DATA
        intent.putExtra(BundleDataInfo.DESIGN_NOTES,upload_notes)
        intent.putExtra(BundleDataInfo.DESIGN_INSTRUCTION,upload_instruction)
        intent.putExtra(BundleDataInfo.DESIGN_FRONT,upload_back)
        intent.putExtra(BundleDataInfo.DESIGN_BACK,upload_back)
    }


    private fun initialise() {

        ll_billinginfor = findViewById(R.id.ll_billinginfor) as LinearLayout

        ll_billinginfor_detailview = findViewById(R.id.ll_billinginfor_detailview) as LinearLayout
        tv_billing_selectprofile = findViewById(R.id.tv_billing_selectprofile) as CustomTextView
        et_billing_name = findViewById(R.id.et_billing_name) as EditText
        et_billing_address = findViewById(R.id.et_billing_address) as EditText
        et_billing_address_2 = findViewById(R.id.et_billing_address_2) as EditText
        et_billinginforcity = findViewById(R.id.et_billinginforcity) as EditText
        tv_billinginforstate = findViewById(R.id.tv_billinginforstate) as CustomTextView
        tv_billinginforzipcode = findViewById(R.id.tv_billinginforzipcode) as EditText
        chk_billing_savenewprofile = findViewById(R.id.chk_billing_savenewprofile) as CheckBox
        chk_billing_setprofiledefault = findViewById(R.id.chk_billing_setprofiledefault) as CheckBox


        ll_shippingaddress = findViewById(R.id.ll_shippingaddress) as LinearLayout

        ll_shippingaddr_detailview = findViewById(R.id.ll_shippingaddr_detailview) as LinearLayout
        tv_shippingaddr_selectprofile = findViewById(R.id.tv_shippingaddr_selectprofile) as CustomTextViewBold
        tv_shippingaddr_addresstype = findViewById(R.id.tv_shippingaddr_addresstype) as CustomTextViewBold
        et_shippingaddr_companyname = findViewById(R.id.et_shippingaddr_companyname) as EditText
        et_shippingaddr_name = findViewById(R.id.et_shippingaddr_name) as EditText
        et_shippingaddr_address = findViewById(R.id.et_shippingaddr_address) as EditText
        et_shippingaddr_address_2 = findViewById(R.id.et_shippingaddr_address_2) as EditText
        et_shippingaddrcity = findViewById(R.id.et_shippingaddrcity) as EditText
        tv_shippingaddrstate = findViewById(R.id.tv_shippingaddrstate) as CustomTextView
        et_shippingaddrzipcode = findViewById(R.id.et_shippingaddrzipcode) as EditText
        chk_shipping_savenewprofile = findViewById(R.id.chk_shipping_savenewprofile) as CheckBox
        chk_shipping_setprofiledefault = findViewById(R.id.chk_shipping_setprofiledefault) as CheckBox


        ll_shipmentinfor = findViewById(R.id.ll_shipmentinfor) as LinearLayout

        ll_shipmentinfor_detailview = findViewById(R.id.ll_shipmentinfor_detailview) as LinearLayout
        et_shipmentinfor_email = findViewById(R.id.et_shipmentinfor_email) as EditText
        et_shipmentinfor_customerrefer = findViewById(R.id.et_shipmentinfor_customerrefer) as EditText
        chk_shipmentinfor_blindshipping = findViewById(R.id.chk_shipmentinfor_blindshipping) as CheckBox

        ll_shipmentmethod = findViewById(R.id.ll_shipmentmethod) as LinearLayout


        ll_selectshipmethod_detailview = findViewById(R.id.ll_selectshipmethod_detailview) as LinearLayout
        tv_shippingmethod = findViewById(R.id.tv_shippingmethod) as CustomTextView


        iv_billinginfor = findViewById(R.id.iv_billinginfor) as ImageView
        iv_shippingaddress = findViewById(R.id.iv_shippingaddress) as ImageView
        iv_shipmentinfor = findViewById(R.id.iv_shipmentinfor) as ImageView
        iv_selectshiipmethod = findViewById(R.id.iv_selectshiipmethod) as ImageView

        btn_revieworder = findViewById(R.id.btn_revieworder) as Button

        radioGroup = findViewById(R.id.ups_radiogroup) as RadioGroup

        ll_billinginfor_detailview.visibility = View.VISIBLE


        radioGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { arg0, selectedId ->
            var selectedId = selectedId

            selectedId = radioGroup.getCheckedRadioButtonId()
            val selectedUPs = findViewById<View>(selectedId) as RadioButton
            val price = selectedUPs.text.toString()
            Log.w("Selected Item","item:::::"+price)

            val parts = price.split(":")
            ups_delevrymethod = parts[0]
            ups_price_shipping = parts[parts.size-1]
            Log.w("Selected Item","item:::::"+ups_delevrymethod)
            Log.w("Selected Item","item:::::"+ups_price_shipping+"ups size"+parts.size)
            if(ups_price_shipping=="" || ups_price_shipping.trim()==""){
                ups_price_shipping = "0.00"
            }


        })

    }


    private fun getStates() {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.getStates(ApiInterface.ENCRIPTION_KEY)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<StatesResponse> {
            override fun onResponse(call: Call<StatesResponse>, response: retrofit2.Response<StatesResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    stringStateArrayList = ArrayList<String>()
                    state_code_array = ArrayList()
                    stringStateArrayList.clear()
                    state_code_array.clear()
                    var list: ArrayList<StatesResponseDetails> = response.body()!!.data!!
                    Log.e("selected_name_id_name", "" + list)
                    for (item: StatesResponseDetails in list.iterator()) {
                        var statename = item.name
                        var stateabbre = item.abbrev
                        stringStateArrayList.add(statename!!)
                        state_code_array.add(stateabbre!!)
                    }
                    //Set the values
                    val set = HashSet<String>()
                    set.addAll(state_code_array)
                    sessionManager.storeList(set)
                    // selected_id = state_code_array[stringStateArrayList.indexOf(selected_name)]
                    // Log.e("selected_name_id_name", selected_id + "\n" + selected_name)
                }
            }

            override fun onFailure(call: Call<StatesResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun getProfileListBilling() {
        val apiService = ApiInterface.create()
        my_loader.show()
        stringaddressArrayList = ArrayList<String>()
        stringaddressArrayList.add(0, "New Profile")
        stringaddressArrayList.add(1, "Default")
        val call = apiService.getProfileList(ApiInterface.ENCRIPTION_KEY, sessionManager.isUserId, "Billing")
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ProfileListResponse> {
            override fun onResponse(call: Call<ProfileListResponse>, response: retrofit2.Response<ProfileListResponse>?) {
                my_loader.dismiss()
                if (response != null) {

                    state_code_array = ArrayList()

                    var list: Array<UserAddressInfo>? = response.body()!!.user_info
                    stringaddressIdBillingList = ArrayList<String>()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {

                        Log.w("Result_Address_LIST", "Result : " + list)
                        for (item: UserAddressInfo in list!!.iterator()) {
                            var profilename = item.name
                            var address_id_billing = item.address_id
                            var company_name = item.company_name
                            var address1 = item.address1
                            var address2 = item.address2
                            var city = item.city
                            var state = item.state
                            var pincode = item.pincode

                            stringaddressArrayList.add(profilename!!)
                            stringaddressIdBillingList.add(address_id_billing!!)

                        }
                    }
                }
            }

            override fun onFailure(call: Call<ProfileListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun getProfileListShipping() {
        val apiService = ApiInterface.create()
        my_loader.show()
        stringaddressArrayList2 = ArrayList<String>()
        stringaddressArrayList2.add(0, "New Profile")
        stringaddressArrayList2.add(1, "Default")
        stringaddressArrayList2.add(2, "Same As Billing")
        val call = apiService.getProfileList(ApiInterface.ENCRIPTION_KEY, sessionManager.isUserId, "Shipping")
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<ProfileListResponse> {
            override fun onResponse(call: Call<ProfileListResponse>, response: retrofit2.Response<ProfileListResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    var list: Array<UserAddressInfo>? = response.body()!!.user_info
                    stringaddressIdShippingList = ArrayList<String>()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {

                        Log.w("Result_Address_LIST", "Result : " + list)
                        for (item: UserAddressInfo in list!!.iterator()) {
                            var profilename = item.name
                            var address_id_billing = item.address_id
                            var company_name = item.company_name
                            var address1 = item.address1
                            var address2 = item.address2
                            var city = item.city
                            var state = item.state
                            var pincode = item.pincode

                            stringaddressArrayList2.add(profilename!!)
                            stringaddressIdShippingList.add(address_id_billing!!)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ProfileListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun getMyProfile() {
        val apiService = ApiInterface.create()
        my_loader.show()

        if(stringaddressArrayList.size>0) {
            address_id_position = stringaddressArrayList.indexOf(selected_profile_name)-2
            //Log.d("REQUEST_LLL",  "---"+stringaddressIdBillingList.get(this!!.address_id_position!!))
        }
        val call = apiService.getMyProfile(ApiInterface.ENCRIPTION_KEY,sessionManager.isUserId, stringaddressIdBillingList.get(this!!.address_id_position!!))
       // Log.d("REQUEST_LLL", call.toString() + "---"+stringaddressIdBillingList.get(this!!.address_id_position!!))
        call.enqueue(object : Callback<NewProfileResponse> {
            override fun onResponse(call: Call<NewProfileResponse>, response: retrofit2.Response<NewProfileResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    //stringaddressArrayList = ArrayList<String>()
                    //state_code_array = ArrayList()
                    // stringaddressArrayList.clear()
                    var list: UserAddressInfo? = response.body()!!.user_info
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {

                        Log.w("Result_getAddress", "Result : " + list)

                        var user_name = list!!.name
                        var user_address_id = list.address_id
                        var user_comp = list.company_name
                        var user_address1 = list.address1
                        var user_address2 = list.address2
                        var user_city = list.city
                        var user_state = list.state
                        var user_pincode = list.pincode
                        et_billing_name.setText(user_name)
                        et_billing_address.setText(user_address1)
                        et_billing_address_2.setText(user_address2)
                        et_billinginforcity.setText(user_city)
                        tv_billinginforstate.setText(user_state)
                        tv_billinginforzipcode.setText(user_pincode)
                    }
                }
            }

            override fun onFailure(call: Call<NewProfileResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }


    private fun getMyProfile_Shipping() {
        val apiService = ApiInterface.create()
        my_loader.show()

        if(stringaddressArrayList2.size>0) {
            address_id_position = stringaddressArrayList2.indexOf(selected_profile_name)-3
            //Log.d("REQUEST_LLL",  "---"+stringaddressIdBillingList.get(this!!.address_id_position!!))
        }
        val call = apiService.getMyProfile(ApiInterface.ENCRIPTION_KEY,sessionManager.isUserId, stringaddressIdShippingList.get(this!!.address_id_position!!))
        // Log.d("REQUEST_LLL", call.toString() + "---"+stringaddressIdBillingList.get(this!!.address_id_position!!))
        call.enqueue(object : Callback<NewProfileResponse> {
            override fun onResponse(call: Call<NewProfileResponse>, response: retrofit2.Response<NewProfileResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    //stringaddressArrayList = ArrayList<String>()
                    //state_code_array = ArrayList()
                    // stringaddressArrayList.clear()
                    var list: UserAddressInfo? = response.body()!!.user_info
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {

                        Log.w("Result_getAddress", "Result : " + list)

                        var user_name = list!!.name
                        var user_address_id = list.address_id
                        var user_comp = list.company_name
                        var user_address1 = list.address1
                        var user_address2 = list.address2
                        var user_city = list.city
                        var user_state = list.state
                        var user_pincode = list.pincode
                        var ship_addresstype = list.shiping_address_type
                        et_shippingaddr_name.setText(user_name)
                        et_shippingaddr_companyname.setText(user_comp)
                        et_shippingaddr_address.setText(user_address1)
                        et_shippingaddr_address_2.setText(user_address2)
                        tv_shippingaddr_addresstype.setText(ship_addresstype)
                        et_shippingaddrcity.setText(user_city)
                        tv_shippingaddrstate.setText(user_state)
                        et_shippingaddrzipcode.setText(user_pincode)
                    }
                }
            }

            override fun onFailure(call: Call<NewProfileResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun getMyDefaultProfile(addresstype: String,status:Boolean) {
        //address should be either Billing or Shipping
        val apiService = ApiInterface.create()
        my_loader.show()

        val call = apiService.getMyDefaultProfile(ApiInterface.ENCRIPTION_KEY,sessionManager.isUserId,addresstype)
//        Log.d("REQUEST_LLL", call.toString() + "---"+stringaddressIdBillingList.get(this!!.address_id_position!!))
        call.enqueue(object : Callback<NewProfileResponse> {
            override fun onResponse(call: Call<NewProfileResponse>, response: retrofit2.Response<NewProfileResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    //stringaddressArrayList = ArrayList<String>()
                    //state_code_array = ArrayList()
                    // stringaddressArrayList.clear()
                    var list: UserAddressInfo? = response.body()!!.user_info
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1")) {

                        Log.w("Result_getAddress", "Result : " + list)

                        var user_name = list!!.name
                        var user_address_id = list.address_id
                        var user_comp = list.company_name
                        var user_address1 = list.address1
                        var user_address2 = list.address2
                        var user_city = list.city
                        var user_state = list.state
                        var user_pincode = list.pincode
                        var ship_addresstype = list.shiping_address_type
                        if(status.equals(true)) {
                            et_billing_name.setText(user_name)
                            et_billing_address.setText(user_address1)
                            et_billing_address_2.setText(user_address2)
                            et_billinginforcity.setText(user_city)
                            tv_billinginforstate.setText(user_state)
                            tv_billinginforzipcode.setText(user_pincode)
                        }else{
                            et_shippingaddr_name.setText(user_name)
                            et_shippingaddr_companyname.setText(user_comp)
                            et_shippingaddr_address.setText(user_address1)
                            et_shippingaddr_address_2.setText(user_address2)
                            tv_shippingaddr_addresstype.setText(ship_addresstype)
                            et_shippingaddrcity.setText(user_city)
                            tv_shippingaddrstate.setText(user_state)
                            et_shippingaddrzipcode.setText(user_pincode)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<NewProfileResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }
    private fun getUPSRating() {
        mUPSProductList.clear()
        mUPSList = ArrayList<String>()
        val gson = Gson()
        val json = sessionManager.getobject
        val json2 = json.toString()
        upsobj = gson.fromJson(json2, UPSObject::class.java)
        if(upsobj.product!= null) {
            mUPSProductList.add(upsobj.product!!)
        }

        if(upsobj != null){

            if(upsobj.zipcode!= null)
                ups_zipcode= upsobj.zipcode!!
            if(upsobj.upsprice!= null)
                upsprice= upsobj.upsprice!!

            if(mUPSProductList.size>0) {
                if(mUPSProductList.get(0).paper_type!= null )
                    ups_papertype = mUPSProductList.get(0).paper_type!!
                if (mUPSProductList.get(0).product_meta != null && mUPSProductList.get(0).product_meta!!.PAGES!= null )
                    ups_pages = mUPSProductList.get(0).product_meta!!.PAGES!!
                if (mUPSProductList.get(0).product_meta != null && mUPSProductList.get(0).product_meta!!.PAPER_OR_STOCK!= null )
                    ups_paperstock = mUPSProductList.get(0).product_meta!!.PAPER_OR_STOCK!!
                if (mUPSProductList.get(0).product_meta != null && mUPSProductList.get(0).product_meta!!.PRINT_SIZE!= null )
                    ups_printsize =  mUPSProductList.get(0).product_meta!!.PRINT_SIZE!!
                if (mUPSProductList.get(0).product_meta != null && mUPSProductList.get(0).product_meta!!.Quantity!= null )
                    ups_quantity = mUPSProductList.get(0).product_meta!!.Quantity!!
                if (mUPSProductList.get(0).product_meta != null && mUPSProductList.get(0).product_meta!!.COVER_PAPER_TYPE!= null )
                    ups_covertype = mUPSProductList.get(0).product_meta!!.COVER_PAPER_TYPE!!

            }
        }

        Log.w("ups data","UPS Values: "+ups_zipcode+","+ups_pages+","+ups_paperstock+","+ups_printsize+","+ups_quantity+","+ups_covertype+","+ups_papertype)
        val apiService = ApiInterface.create()
        my_loader.show()

        if(ups_zipcode=="" || ups_zipcode==null){
            ups_zipcode = et_shippingaddrzipcode.text.toString()
        }

        val call = apiService.getUPSRates(ApiInterface.ENCRIPTION_KEY,ups_zipcode,ups_pages,ups_paperstock,ups_printsize,ups_papertype,ups_quantity,ups_covertype)
        call.enqueue(object : Callback<UPSResponse> {
            override fun onResponse(call: Call<UPSResponse>, response: retrofit2.Response<UPSResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    mUPSList = ArrayList<String>()
                    Log.w("ups data response", "Result : success"+response.body()!!.status)
                    Log.w("response", GsonBuilder().setPrettyPrinting().create().toJson(response.body()))
                    if(response.body()!!.data != null) {
                        val data_string = GsonBuilder().setPrettyPrinting().create().toJson(response.body()!!.data)
                        val jobject = JSONObject(data_string)
                        val data_keys = jobject.keys()

                        while (data_keys.hasNext()) {
                            val key = data_keys.next()
                            val value = jobject.optString(key)
                            Log.i("TAG", "key:" + key + "--Value::" + jobject.optString(key))
                            mUPSList.add(key + ":" + value)
                        }
                    }



                    setDatatoRadioGroup(mUPSList)
                }

            }

            override fun onFailure(call: Call<UPSResponse>, t: Throwable) {
                Log.w("ups data Resp", "Result : Failed")
            }
        })
    }

    private fun setDatatoRadioGroup(mUPSList: ArrayList<String>) {

        var rprms: RadioGroup.LayoutParams
        radioGroup.removeAllViews()
        for (i in 1 until mUPSList.size) {
            val radioButton = RadioButton(this)
            radioButton.text = mUPSList.get(i)
            rprms = RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT)
            radioGroup.addView(radioButton, rprms)
        }
    }




    private fun hasValidCredentialsBilling(): Boolean {
        if (TextUtils.isEmpty(billing_name)) {
            Toast.makeText(this, "Please Enter Name", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(billing_address)) {
            Toast.makeText(this, "Please Enter Address", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(billing_city)) {
            Toast.makeText(this, "Please Enter City", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(billing_state)) {
            Toast.makeText(this, "Please Enter State", Toast.LENGTH_SHORT).show()
        } else if (billing_state.equals("--Select State--")) {
            Toast.makeText(this, "Please Enter State", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(billing_zipcode)) {
            Toast.makeText(this, "Please Enter Zipcode", Toast.LENGTH_SHORT).show()
        } else {
            return true
        }
        return false
    }


    private fun hasValidCredentialsShipping(): Boolean {
        if (TextUtils.isEmpty(shipping_company_name)) {
            Toast.makeText(this, "Please Company Name", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(shipping_name)) {
            Toast.makeText(this, "Please Enter Name", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(shipping_address)) {
            Toast.makeText(this, "Please Enter Address", Toast.LENGTH_SHORT).show()
        }  else if (TextUtils.isEmpty(shipping_city)) {
            Toast.makeText(this, "Please Enter City", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(shipping_state)) {
            Toast.makeText(this, "Please Enter State", Toast.LENGTH_SHORT).show()
        } else if (shipping_state.equals("--Select State--")) {
            Toast.makeText(this, "Please Enter State", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(shipping_zipcode)) {
            Toast.makeText(this, "Please Enter Zipcode", Toast.LENGTH_SHORT).show()
        } else {
            return true
        }
        return false
    }


    private fun hasValidCredentialsShipment(): Boolean {
        if (TextUtils.isEmpty(shipment_email)) {
            Toast.makeText(this, "Please Enter Alternate Email", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(shipment_reference)) {
            Toast.makeText(this, "Please Enter Customer Reference", Toast.LENGTH_SHORT).show()
        } else {
            return true
        }
        return true
    }
    private fun hasValidUPS(): Boolean {
        if (TextUtils.isEmpty(tv_shippingmethod.text.toString())) {
            Toast.makeText(this, "Please Select Shipping method", Toast.LENGTH_SHORT).show()
        } else {
            return true
        }
        return false
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.ll_order -> {
            }
            R.id.ll_shipping -> {
            }
            R.id.ll_review -> {
            }
            R.id.ll_upload -> {
            }
        }
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

}
