package com.indobytes.a4printuser.Helper;

/**
 * Created by inforlinx on 12/4/15.
 */
public interface WebServiceCallBack {


    void onJSONResponse(String jsonResponse, String type);

    void onFailure();
}
