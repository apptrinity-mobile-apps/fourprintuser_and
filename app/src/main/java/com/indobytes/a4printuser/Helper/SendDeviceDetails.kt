package com.indobytes.a4printuser.Helper

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import com.indobytes.a4printuser.MainActivity
import com.indobytes.a4printuser.ReviewActivity
import com.indobytes.a4printuser.model.ApiInterface
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.URL
import android.app.ListActivity
import android.widget.Toast


class SendDeviceDetails() : AsyncTask<String, Void, String>(){

    var context: Context? = null

    // Secondary Constructor
    constructor(reviewActivity: ReviewActivity): this()  {
        this.context = reviewActivity
    }

    override fun doInBackground(vararg params: String): String {

        var data = ""

        var httpURLConnection: HttpURLConnection? = null
        try {

            httpURLConnection = URL(params[0]).openConnection() as HttpURLConnection
            httpURLConnection!!.setRequestMethod("POST")
            httpURLConnection!!.setRequestProperty("Content-Type", "application/json")
            httpURLConnection!!.setRequestProperty("encryption_key",ApiInterface.ENCRIPTION_KEY)
            httpURLConnection!!.setDoOutput(true)

            val wr = DataOutputStream(httpURLConnection!!.getOutputStream())
            wr.writeBytes(params[1])
            wr.flush()
            wr.close()

            val `in` = httpURLConnection!!.getInputStream()
            val inputStreamReader = InputStreamReader(`in`)

            var inputStreamData = inputStreamReader.read()
            while (inputStreamData != -1) {
                val current = inputStreamData.toChar()
                inputStreamData = inputStreamReader.read()
                data += current
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection!!.disconnect()
            }
        }

        return data
    }

    override fun onPostExecute(result: String) {
        super.onPostExecute(result)
        try{
            val jsonobj = org.json.JSONObject(result)
            Log.e("TAG-JSON","Response"+result)

            val status = jsonobj.getString("status")
            Log.e("TAG-JSON","status"+status)
            if(status.equals("1")){

                val intent = Intent(context, MainActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                context!!.applicationContext.startActivity(intent)
                Toast.makeText(context,"Order Saved Successfully",Toast.LENGTH_SHORT).show()
            }
        }catch (e:Exception){
            e.printStackTrace()
        }

    }


}