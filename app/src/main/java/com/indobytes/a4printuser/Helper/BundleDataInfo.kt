package com.indobytes.a4printuser.Helper

class BundleDataInfo {

    companion object {
        //shipping billing variables

        val BILLING_INFO_SELECT_PROFILE = "billing_profile"
        val BILLING_INFO_NAME = "billing_name"
        val BILLING_INFO_ADDRESS = "billing_address"
        val BILLING_INFO_ADDRESS2 = "billing_address2"
        val BILLING_INFO_CITY = "billing_city"
        val BILLING_INFO_STATE = "billing_state"
        val BILLING_INFO_ZIPCODE = "billing_zipcode"
        val BILLING_INFO_SAVE_NEW_PROFILE = "billing_new_profile"
        val BILLING_INFO_SAVE_PROFILEAS_DEFAULT = "billing_default_profile"

        //shipping address varibales

        val SHIPPING_ADDRESS_SELECT_PROFILE = "shipping_addr_profile"
        val SHIPPING_ADDRESS_TYPE = "shipping_addr_type"
        val SHIPPING_ADDRESS_COMPANY_NAME = "shipping_addr_company_name"
        val SHIPPING_ADDRESS_NAME = "shipping_addr_name"
        val SHIPPING_ADDRESS_ADDRESS = "shipping_addr_address"
        val SHIPPING_ADDRESS_ADDRESS2 = "shipping_addr_address2"
        val SHIPPING_ADDRESS_CITY = "shipping_addr_city"
        val SHIPPING_ADDRESS_STATE = "shipping_addr_state"
        val SHIPPING_ADDRESS_ZIPCODE = "shipping_addr_zipcode"
        val SHIPPING_ADDRESS_SAVE_NEW_PROFILE = "shipping_addr_new_profile"
        val SHIPPING_ADDRESS_SAVE_PROFILEAS_DEFAULT = "shipping_addr_default_profile"

        //shipping information

        val SHIPPING_INFO_EMAIL = "shipping_info_email"
        val SHIPPING_INFO_CC_REFER = "shipping_cc_refer"
        val SHIPPING_INFO_BLIND_SHIPPING = "shipping_blind_shipping"

        //shipping method

        val SHIPPING_METHOD = "shipping_method"
        val SHIPPING_METHOD_ITEM_PRICE = "shipping_method_item_price"
        val SHIPPING_METHOD_ITEM_DELIVERYMETHOD = "shipping_method_delivery"


        val DESIGN_NOTES = "desing_notes"
        val DESIGN_FRONT = "desing_front"
        val DESIGN_BACK = "desing_back"
        val DESIGN_INSTRUCTION = "desing_instruction"


    }
}
