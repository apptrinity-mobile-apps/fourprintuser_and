package com.indobytes.a4print.Session

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import com.indobytes.a4printuser.LoginActivity
import org.json.JSONObject
import java.util.*


/**
 * Created by indobytes21 on 2/9/2017.
 */

class SessionManager// Constructor
(// Context
        internal var _context: Context) {
    internal var pref: SharedPreferences
    // Editor for Shared preferences
    internal var editor: SharedPreferences.Editor
    // Shared pref mode
    internal var PRIVATE_MODE = 0
    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGIN, false)
    val isUserId :String
        get() = pref.getString(PROFILE_ID,"")
    val isUserName :String
        get() = pref.getString(PROFILE_USERNAME,"User Name")
    val states : MutableSet<String>?
        get() = pref.getStringSet(STATES_LIST,null)
    val fileuploadimagelist : MutableSet<String>?
        get() = pref.getStringSet(FILE_IMAGE_LIST,null)
    val getobject: String
        get() = pref.getString("json_response","")
    // user name
    // user email id
    //user.put(PROFILE_WEB_ID, pref.getString(PROFILE_WEB_ID, null));
    // return user
    val userDetails: HashMap<String, String>
        get() {
            val user = HashMap<String, String>()
            user[PROFILE_ID] = pref.getString(PROFILE_ID, null)
            user[PROFILE_EMAIL] = pref.getString(PROFILE_EMAIL, null)
            user[PROFILE_USERNAME] = pref.getString(PROFILE_USERNAME, null)
            user[PROFILE_USER_FIRST_NAME] = pref.getString(PROFILE_USER_FIRST_NAME, null)
            user[PROFILE_LAST_NAME] = pref.getString(PROFILE_LAST_NAME, null)
            user[PROFILE_COMPANY] = pref.getString(PROFILE_COMPANY, null)
            user[PROFILE_PHONE_NUMBER] = pref.getString(PROFILE_PHONE_NUMBER, null)
            user[PROFILE_ALTERNATE_NUMBER] = pref.getString(PROFILE_ALTERNATE_NUMBER, null)
            user[PROFILE_CUSTOMER_SERVICE] = pref.getString(PROFILE_CUSTOMER_SERVICE, null)
            user[PROFILE_REFFERD_BY] = pref.getString(PROFILE_REFFERD_BY, null)
            return user
        }

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun createLoginSession(userid: String, first_name: String, last_name: String, username: String, email: String, company: String, phone_number: String, alter_phone: String, cust_service: String, ref_by: String) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true)
        // Storing name in pref
        editor.putString(PROFILE_ID, userid)
        editor.putString(PROFILE_EMAIL, email)
        editor.putString(PROFILE_USERNAME, username)
        editor.putString(PROFILE_USER_FIRST_NAME, first_name)
        editor.putString(PROFILE_LAST_NAME, last_name)
        editor.putString(PROFILE_COMPANY, company)
        editor.putString(PROFILE_PHONE_NUMBER, phone_number)
        editor.putString(PROFILE_ALTERNATE_NUMBER, alter_phone)
        editor.putString(PROFILE_CUSTOMER_SERVICE, cust_service)
        editor.putString(PROFILE_REFFERD_BY, ref_by)
        // commit changes
        editor.commit()
    }

    fun checkLogin() {
        // Check login status
        if (!this.isLoggedIn) {
            // user is not logged in redirect him to Login Activity
            val i = Intent(_context, LoginActivity::class.java)
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            // Add new Flag to start new Activity
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            // Staring Login Activity
            _context.startActivity(i)
        }
    }

    fun logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear()
        editor.commit()
        // After logout redirect user to Loing Activity
        val i = Intent(_context, LoginActivity::class.java)
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        // Add new Flag to start new Activity
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        // Staring Login Activity
        _context.startActivity(i)
    }

    fun storeList(set: HashSet<String>) {
        editor.putStringSet(STATES_LIST, set)
        editor.commit()

    }

    fun setUserObject( userObject: String, key: String) {
        Log.w("object data ","Data: "+userObject)
        editor.putString(key, userObject)
        editor.commit()
    }
    fun storeFileImages(set: HashSet<String>) {
        editor.putStringSet(FILE_IMAGE_LIST, set)
        editor.commit()

    }
    val sublabel: java.util.ArrayList<String> = java.util.ArrayList<String>()
    val extrascharge: java.util.ArrayList<String> = java.util.ArrayList<String>()
    val addoptional: java.util.ArrayList<String> = java.util.ArrayList<String>()
    fun parser(extracharge: String) {
        val jsonobj = JSONObject(extracharge)
        if(extracharge.contains("sublabels")) {
            val product = jsonobj.getJSONObject("product")
            val mainObject2 = product.getJSONObject("sublabels")

                val data_keys = mainObject2.keys()

                while (data_keys.hasNext()) {
                    val key = data_keys.next()
                    val value = mainObject2.optString(key)
                    Log.i("Review  session2", "key:" + key + "--Value::" + value)
                    sublabel.add(key+":"+value)
            }
        }
        if(extracharge.contains("ExtraCharges")) {
            val product = jsonobj.getJSONObject("product")
            val mainObject = product.getJSONArray("ExtraCharges")
            for (i in 0 until mainObject.length()) {
                val uniObject = mainObject.getJSONObject(i)
                val data_keys = uniObject.keys()

                while (data_keys.hasNext()) {
                    val key = data_keys.next()
                    val value = uniObject.optString(key)
                    Log.i("Review  session1", "key:" + key + "--Value::" + value)
                    extrascharge.add(key+":"+value)
                }
            }
        }
        if(extracharge.contains("AddOptionals")) {
            val product = jsonobj.getJSONObject("product")
            val mainObject3 = product.getJSONArray("AddOptionals")
            for (i in 0 until mainObject3.length()) {
                val uniObject = mainObject3.getJSONObject(i)
                val data_keys = uniObject.keys()

                while (data_keys.hasNext()) {
                    val key = data_keys.next()
                    val value = uniObject.optString(key)
                    Log.i("Review  session3", "key:" + key + "--Value::" + value)
                    addoptional.add(key+":"+value)
                }
            }
        }

    }


    companion object {

        var PROFILE_ID = "userId"
        var PROFILE_EMAIL = "email"
        var PROFILE_USERNAME = "username"
        var PROFILE_USER_FIRST_NAME = "first_name"
        var PROFILE_LAST_NAME = "last_name"
        var PROFILE_COMPANY = "company"
        var PROFILE_PHONE_NUMBER = "phone_number"
        var PROFILE_ALTERNATE_NUMBER = "alternate_no"
        var PROFILE_CUSTOMER_SERVICE = "service"
        var PROFILE_REFFERD_BY = "refferd_by"
        var IS_LOGIN = "IsLoggedIn"
        var PREF_NAME = "4print"

        var STATES_LIST = "states"
        var FILE_IMAGE_LIST = "imagelist"
    }
}
