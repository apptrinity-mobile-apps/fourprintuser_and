package com.indobytes.a4printuser.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.indobytes.a4printuser.fragment.UserOrderAllFragment
import com.indobytes.a4printuser.fragment.UserOrderCompletedFragment
import com.indobytes.a4printuser.fragment.UserOrderProductionFragment
import com.indobytes.a4printuser.fragment.UserOrderRecentFragment

class PageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private val tabNames: ArrayList<String>
    private val fragments: ArrayList<Fragment>
    private val tabTitles = arrayOf("RECENT", "PRODUCTION", "COMPLETED","ALL")

    init {
        tabNames = ArrayList()
        fragments = ArrayList()
    }

    fun add(fragment: Fragment, title: String) {
        tabNames.add(title)
        fragments.add(fragment)
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return UserOrderRecentFragment()
            1 -> return UserOrderProductionFragment()
            2 -> return UserOrderCompletedFragment()
            3 -> return UserOrderAllFragment()
        }
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence {
        return tabTitles[position];
    }


}