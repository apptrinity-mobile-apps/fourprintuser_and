package com.indobytes.a4printuser.adapter

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.indobytes.a4printuser.OrderDetailsActivity
import com.indobytes.a4printuser.R
import com.indobytes.a4printuser.model.APIResponse.ProductsResponseDetails
import com.indobytes.a4printuser.model.APIResponse.UserOrderInfo
import com.indobytes.a4printuser.model.ApiInterface
import com.squareup.picasso.Picasso

data class OrderDetailsAdapter(val mUserOrderListInfo: ArrayList<UserOrderInfo>, val activity: Activity) : BaseAdapter(){
    private var order_id_txt_view: TextView? = null
    private var order_status_txt_view: TextView? = null
    private var order_date_txt_view: TextView? = null
    private var order_img_view: ImageView? = null
    private var order_title_txt_view:TextView?=null
    private var order_no_info_view:TextView?=null
    private var ll_order_all_status:LinearLayout?=null

    override fun getItem(position: Int): Any {
        return mUserOrderListInfo.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mUserOrderListInfo.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(activity, R.layout.fragment_user_order,null)

        order_id_txt_view = view!!.findViewById(R.id.order_id) as TextView
        order_status_txt_view = view!!.findViewById(R.id.order_status) as TextView
        order_date_txt_view = view!!.findViewById(R.id.order_date) as TextView
        order_img_view = view!!.findViewById(R.id.order_image) as ImageView
        order_title_txt_view = view!!.findViewById(R.id.order_title) as TextView

        order_no_info_view = view!!.findViewById(R.id.tv_no_info) as TextView
        ll_order_all_status = view!!.findViewById(R.id.ll_order_all_status) as LinearLayout


        if(mUserOrderListInfo != null && mUserOrderListInfo.size>0) {
            order_id_txt_view!!.setText("#" + mUserOrderListInfo.get(position).order_id)
            Log.e("Order id", "" + mUserOrderListInfo.get(position).order_id)
            order_status_txt_view!!.setText(mUserOrderListInfo.get(position).order_status)
            order_date_txt_view!!.setText(mUserOrderListInfo.get(position).created_on)
            order_title_txt_view!!.setText(mUserOrderListInfo.get(position).product_name)


            val path = mUserOrderListInfo.get(position).image

            Picasso.with(activity).load(ApiInterface.IMAGE_BASE_ORDER_URL + path).into(order_img_view)

            view.setOnClickListener(View.OnClickListener {

                val intent = Intent(this.activity, OrderDetailsActivity::class.java)
                intent.putExtra("order_id", mUserOrderListInfo.get(position).order_id)
                intent.putExtra("product_name", mUserOrderListInfo.get(position).product_name)
                intent.putExtra("image_url", ApiInterface.IMAGE_BASE_ORDER_URL + path)
                activity.startActivity(intent)
            })
        }else{
            order_no_info_view!!.visibility = View.VISIBLE
            order_no_info_view!!.setText("No Orders Found")
            ll_order_all_status!!.visibility = View.GONE
        }

        return view
    }

}
