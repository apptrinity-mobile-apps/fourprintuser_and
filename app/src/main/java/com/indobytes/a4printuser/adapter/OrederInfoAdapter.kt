package com.indobytes.a4printuser.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.indobytes.a4printuser.Helper.CustomTextView
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import com.indobytes.a4printuser.R
import com.indobytes.a4printuser.model.APIResponse.OrderData


class OrederInfoAdapter(private val mContext: Context, internal var ordersarraylist: OrderData?) : RecyclerView.Adapter<OrederInfoAdapter.MyViewHolder>() {

    inner class MyViewHolder(rowView: View) : RecyclerView.ViewHolder(rowView) {
        internal var tv_key_name_id: CustomTextViewBold
        internal var tv_value_name_id: CustomTextView
        internal var ll_order_info: LinearLayout


        init {

            tv_key_name_id = rowView.findViewById(R.id.tv_order_info_item) as CustomTextViewBold
            tv_value_name_id = rowView.findViewById(R.id.tv_order_info_item_value) as CustomTextView
            ll_order_info = rowView.findViewById(R.id.ll_order_info_adapter) as LinearLayout


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.order_info_item, parent, false)

        return MyViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        try {

            if(position%2==0){
                holder.ll_order_info.setBackgroundColor(mContext.resources.getColor(R.color.white))
            }else {
                holder.ll_order_info.setBackgroundColor(mContext.resources.getColor(R.color.cyan_list_bg))

            }
            holder.tv_key_name_id.setText(ordersarraylist!!.productMeta!!.get(position).meta_key)
            holder.tv_value_name_id.setText(ordersarraylist!!.productMeta!!.get(position).meta_value)

        } catch (e: Exception) {
            Log.e("AdapterError", "AdapterError")
        }

    }

    override fun getItemCount(): Int {
        return ordersarraylist!!.productMeta!!.size
    }
}

