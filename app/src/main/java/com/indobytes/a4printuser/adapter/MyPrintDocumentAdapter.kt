package com.indobytes.a4printuser.adapter

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import android.os.ParcelFileDescriptor
import android.print.PageRange
import android.print.PrintAttributes
import android.print.PrintDocumentAdapter
import android.support.annotation.RequiresApi
import com.indobytes.a4printuser.OrderDetailsActivity
import android.print.PrintDocumentInfo
import android.print.pdf.PrintedPdfDocument
import com.indobytes.a4printuser.ReviewActivity


@TargetApi(Build.VERSION_CODES.KITKAT)
class MyPrintDocumentAdapter() : PrintDocumentAdapter() {

    var context: Context? = null

    // Secondary Constructor
    constructor(orderDetailsActivity: OrderDetailsActivity): this()  {
        this.context = orderDetailsActivity
    }

    override fun onWrite(pages: Array<out PageRange>?, destination: ParcelFileDescriptor?, cancellationSignal: CancellationSignal?, callback: WriteResultCallback?) {

    }

    override fun onLayout(oldAttributes: PrintAttributes?, newAttributes: PrintAttributes?, cancellationSignal: CancellationSignal?, callback: LayoutResultCallback?, extras: Bundle?) {
        // Create a new PdfDocument with the requested page attributes
        var mPdfDocument = PrintedPdfDocument(context, newAttributes)

        // Respond to cancellation request
        if (cancellationSignal!!.isCanceled()) {
            callback!!.onLayoutCancelled()
            return
        }

        // Compute the expected number of printed pages
        val pages = computePageCount(newAttributes)

        if (pages > 0) {
            // Return print information to print framework
            val info = PrintDocumentInfo.Builder("print_output.pdf")
                    .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                    .setPageCount(pages)
                    .build()
            // Content layout reflow is complete
            callback!!.onLayoutFinished(info, true)
        } else {
            // Otherwise report an error to the print framework
            callback!!.onLayoutFailed("Page count calculation failed.")
        }
    }

    private fun computePageCount(newAttributes: PrintAttributes?): Int {
        var itemsPerPage = 4 // default item count for portrait mode

        val pageSize = newAttributes!!.getMediaSize()
        if (!pageSize.isPortrait()) {
            // Six items per page in landscape orientation
            itemsPerPage = 6
        }

        // Determine number of print items
        val printItemCount = pageSize

        return 1
    }

}
