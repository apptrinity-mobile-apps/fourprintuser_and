package com.indobytes.a4printuser.adapter

import android.app.Activity
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.indobytes.a4printuser.OrderActivity
import com.indobytes.a4printuser.R
import com.indobytes.a4printuser.model.APIResponse.ProductsResponseDetails
import com.indobytes.a4printuser.model.ApiInterface
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.nav_header_main.view.*


data class ProductDetailsAdapter(val mProductList: ArrayList<ProductsResponseDetails>, val activity: Activity) : BaseAdapter(){



    override fun getItem(position: Int): Any {
        return mProductList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mProductList.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(activity,R.layout.products_list_adapter_layout,null)

        val mProductImage = view.findViewById<ImageView>(R.id.product_image) as ImageView
        val mProductItem = view.findViewById<TextView>(R.id.product_name) as TextView


        mProductItem.text= mProductList.get(position).name
        val path=mProductList.get(position).image
        //mProductImage.setImageResource(load_pic)

        Picasso.with(activity).load(ApiInterface.IMAGE_BASE_URL+path).into(mProductImage)

        view.setOnClickListener(View.OnClickListener {
            val intent = Intent(this.activity,OrderActivity::class.java)
            intent.putExtra("product_id",mProductList.get(position).id)
            intent.putExtra("product_name",mProductList.get(position).name)
            intent.putExtra("product_image",ApiInterface.IMAGE_BASE_URL+path)
            intent.putExtra("product_desc",mProductList.get(position).product_description)
            activity.startActivity(intent)
        })
        return view
    }

}
