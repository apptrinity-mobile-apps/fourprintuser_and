package com.indobytes.a4printuser

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.afollestad.materialdialogs.MaterialDialog
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.Filechooser.FileUtils
import com.indobytes.a4printuser.Helper.BundleDataInfo
import com.indobytes.a4printuser.Helper.FileManager
import com.indobytes.a4printuser.model.APIResponse.FileUploadResponse
import com.indobytes.a4printuser.model.ApiInterface
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import java.io.File
import java.util.*


class UploadActivity : AppCompatActivity() , View.OnClickListener {

    internal lateinit var up_file_info: LinearLayout
    internal lateinit var up_file_info_details: LinearLayout
    internal lateinit var up_file_upload: LinearLayout
    internal lateinit var up_file_upload_details: LinearLayout
    var dialog_list: Dialog?=null
    internal lateinit var up_file_info_design_txt: TextView
    internal lateinit var upload_instruction_txt: TextView
    internal lateinit var up_instruction: TextView
    internal lateinit var up_fronttext: TextView
    internal lateinit var up_backtext: TextView
    internal lateinit var fup_file_add_txtview: TextView
    internal lateinit var fup_file_start_txtview: TextView


    internal lateinit var btn_previous_step: TextView
    internal lateinit var btn_continue_step: TextView

    internal lateinit var ll_instruction: LinearLayout
    internal lateinit var ll_backtext: LinearLayout
    internal lateinit var ll_fronttext: LinearLayout
    internal lateinit var ll_notes: LinearLayout


    internal lateinit var up_file_info_notes_txt: EditText
    internal lateinit var fup_file_filedata_txtview:ListView
    val design = listOf("Upload My Art Work", "Standard Custom Design", "Rush Custom Design")
    var list_items_list: ListView? = null
    internal lateinit var cust_design_selected_stg: String
    private val FILE_SELECT_CODE = 0
    var product_name: String = ""
    var product_id: String = ""
    var product_image: String = ""
    internal lateinit var mediaPath:String

    private var mOutputUri: Uri? = null
    val CAMERA_IMAGE_CODE = 0
    internal val PERMISSION_REQUEST_CODE = 111

    var url: Uri? = null
    var path: String? = null

    var upload_notes: String = ""
    var upload_instruction: String = ""
    var upload_front: String = ""
    var upload_back: String = ""

    internal lateinit var ll_order:LinearLayout
    internal lateinit var ll_upload:LinearLayout
    internal lateinit var ll_shipping:LinearLayout
    internal lateinit var ll_review:LinearLayout

    internal lateinit var imageList:ArrayList<String>
    internal lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setTitle("UPLOAD")
        sessionManager = SessionManager(this)

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(
        builder.build())

        if (intent !=null && intent.getStringExtra("product_id")!=null) {
            product_id = intent.getStringExtra("product_id")
            product_name = intent.getStringExtra("product_name")
            product_image = intent.getStringExtra("product_image")
            Log.e("product_id", product_id + "----" + product_name + "-----" + product_image)
        }

        ll_order = findViewById(R.id.ll_order) as LinearLayout
        ll_upload = findViewById(R.id.ll_upload) as LinearLayout
        ll_shipping = findViewById(R.id.ll_shipping) as LinearLayout
        ll_review = findViewById(R.id.ll_review) as LinearLayout

        ll_upload.setOnClickListener(this)
        ll_shipping.setOnClickListener(this)
        ll_review.setOnClickListener(this)
        ll_order.setOnClickListener(this)


        dialog_list = Dialog(this)
        dialog_list!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_list!!.setContentView(R.layout.dialog_view)
        dialog_list!!.setCancelable(true)
        list_items_list = dialog_list!!.findViewById(R.id.list_languages)

        up_file_info = findViewById(R.id.ll_fileinfo) as LinearLayout
        up_file_info_details = findViewById(R.id.ll_fileinfo_view) as LinearLayout
        up_file_upload = findViewById(R.id.ll_fileupload) as LinearLayout
        up_file_upload_details = findViewById(R.id.ll_file_upload_view) as LinearLayout

        up_file_info_design_txt = findViewById(R.id.up_fileinfo_design_txt) as TextView
        upload_instruction_txt = findViewById(R.id.upload_instruction_txt) as TextView

        val text = "<Html><p style=\"text-align:justify\"><font color=\"#FC4A86\"><b>Instructions:</b></font><font color=\"#000000\"> Please upload the Front \\u0026 Back Image of your product. The preferred file specification are 300dpi CMYK JPEG at High Quality with a .125* Bleed.</font></p></Html>"
        upload_instruction_txt.setText(Html.fromHtml(text))

        up_instruction = findViewById(R.id.up_instruction) as EditText
        up_fronttext = findViewById(R.id.up_fronttext) as EditText
        up_backtext = findViewById(R.id.up_backtext) as EditText
        up_file_info_notes_txt = findViewById(R.id.up_fileinfo_notes_txt) as EditText
        fup_file_add_txtview = findViewById(R.id.fup_add_files_txt) as TextView
        fup_file_start_txtview = findViewById(R.id.fup_upload_files_txt) as TextView
        fup_file_filedata_txtview = findViewById(R.id.fup_file_data) as ListView

        btn_previous_step = findViewById(R.id.btn_previousstep) as TextView
        btn_continue_step = findViewById(R.id.btn_continuestep) as TextView

        ll_instruction = findViewById(R.id.ll_instruction) as LinearLayout
        ll_backtext = findViewById(R.id.ll_backtext) as LinearLayout
        ll_fronttext = findViewById(R.id.ll_fronttext) as LinearLayout
        ll_notes = findViewById(R.id.ll_notes) as LinearLayout
        myloading()
        imageList = arrayListOf<String>()

        up_file_info_details.visibility = View.VISIBLE
        up_file_upload_details.visibility = View.GONE


        up_file_info.setOnClickListener(View.OnClickListener {
            up_file_info_details.visibility = View.VISIBLE
            up_file_upload_details.visibility = View.GONE

        })
        up_file_upload.setOnClickListener(View.OnClickListener {
            up_file_info_details.visibility = View.GONE
            up_file_upload_details.visibility = View.VISIBLE

        })

        up_file_info_design_txt.setOnClickListener(View.OnClickListener {
            if (!dialog_list!!.isShowing())
                dialog_list!!.show()
            val state_array_adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, design)
            list_items_list!!.setAdapter(state_array_adapter)
            list_items_list!!.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
                cust_design_selected_stg = list_items_list!!.getItemAtPosition(i).toString()
                up_file_info_design_txt.setText(cust_design_selected_stg)
                if(cust_design_selected_stg.equals("Upload My Art Work")){
                    ll_backtext.visibility = View.GONE
                    ll_fronttext.visibility = View.GONE
                    ll_instruction.visibility = View.GONE
                }else{
                    ll_backtext.visibility = View.VISIBLE
                    ll_fronttext.visibility = View.VISIBLE
                    ll_instruction.visibility = View.VISIBLE
                }
                dialog_list!!.dismiss()
            })
        })
        fup_file_add_txtview.setOnClickListener(View.OnClickListener {
            showFileChooser()
        })
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCameraPermission() || !checkReadExternalStoragePermission() || !checkWriteExternalStoragePermission()) {
                requestPermission()
            } else {

            }
        } else {

        }
        btn_continue_step.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@UploadActivity,ShippingActivity::class.java)
            putBundleData(intent)
            startActivity(intent)
        })
        btn_previous_step.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

    }

    private fun putBundleData(intent: Intent) {
        upload_notes = up_file_info_notes_txt.text.toString()
        upload_back = up_backtext.text.toString()
        upload_instruction = up_instruction.text.toString()
        upload_front = up_fronttext.text.toString()

        intent.putExtra(BundleDataInfo.DESIGN_NOTES,upload_notes)
        intent.putExtra(BundleDataInfo.DESIGN_INSTRUCTION,upload_instruction)
        intent.putExtra(BundleDataInfo.DESIGN_FRONT,upload_back)
        intent.putExtra(BundleDataInfo.DESIGN_BACK,upload_back)
    }

    private fun checkCameraPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkReadExternalStoragePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkWriteExternalStoragePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
    }



    private fun showFileChooser() {
        MaterialDialog.Builder(this@UploadActivity).title("Choose file")
                .items(*CHOOSE_FILE)
                .itemsCallback { materialDialog, view, i, charSequence ->
                    if (i == 0) {
                        //Mengambil foto dengan camera
                        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                            mOutputUri = FileManager.getOutputMediaFileUri(CAMERA_IMAGE_CODE)
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputUri)
                            startActivityForResult(intent, 100)


                    } else {

                        // Use the GET_CONTENT intent from the utility class
                        val target = FileUtils.createGetContentIntent()
                        // Create the chooser Intent
                        val intent = Intent.createChooser(
                                target, resources.getString(R.string.chooser_title))
                        try {
                            startActivityForResult(intent, 200)
                        } catch (e: ActivityNotFoundException) {
                            // The reason for the existence of aFileChooser
                        }

                    }
                }.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {

            val single_path = FileManager.getPath(applicationContext, requestCode, mOutputUri)

            val file = File(single_path)

            fileuploadserver(file)



        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {

           /* val uri = data!!.getData();
            Log.i("Uri: ", "Uri: " + uri.toString());


            val selectedImage = data!!.getData()
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

            val cursor = contentResolver.query(selectedImage!!, filePathColumn, null, null, null)!!
            cursor.moveToFirst()

            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
            mediaPath = cursor.getString(columnIndex)

            cursor.close()
            val file = File(mediaPath)
            fileuploadserver(file)*/

            val uri = data!!.getData()
            Log.i("URI", "Uri = " + uri!!.toString())
            try {
                // Get the file path from the URI
                val path = FileUtils.getPath(this, uri)
                val file = File(path)
                fileuploadserver(file)
               /* Toast.makeText(this@UploadActivity,
                        "File Selected: $path", Toast.LENGTH_LONG).show()*/
            } catch (e: Exception) {
                Log.w("FileSelectorTest", "File select error", e)
            }

        }



    }


    private fun fileuploadserver(image_name: File?) {
        val apiService = ApiInterface.create()
        my_loader.show()
        val requestBody = RequestBody.create(MediaType.parse("*/*"), image_name)
        val fileToUpload = MultipartBody.Part.createFormData("file_name", image_name!!.getName(), requestBody)

        val call = apiService.fileUpload(ApiInterface.ENCRIPTION_KEY,fileToUpload)
        Log.d("REQUEST", call.toString() + "--"+fileToUpload)
        call.enqueue(object : Callback<FileUploadResponse> {
            override fun onResponse(call: Call<FileUploadResponse>, response: retrofit2.Response<FileUploadResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_File " +
                            "","Result : "+response.body()!!.data+"--"+response.body()!!.result)
                    if(response.body()!!.status.equals(
                                    "1") && response.body()!!.result!=null) {
                        Toast.makeText(applicationContext, response.body()!!.result, Toast.LENGTH_SHORT).show()
                        imageList!!.add(response.body()!!.data!!.upload_filename.toString())
                        val adapter = UserListAdapter(this@UploadActivity, imageList)

                        fup_file_filedata_txtview?.adapter = adapter
                        adapter?.notifyDataSetChanged()

                        val set = HashSet<String>()
                        set.addAll(imageList)
                        sessionManager.storeFileImages(set)
                        //onBackPressed()
                    }else{
                        Toast.makeText(applicationContext, response.body()!!.result, Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<FileUploadResponse>, t: Throwable) {
                Log.w("Result_File Upload",t)
            }
        })

    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.ll_order -> {
            }
            R.id.ll_shipping -> {

            }
            R.id.ll_review -> {

            }
            R.id.ll_upload -> {
            }
        }
    }


    override fun onBackPressed() {

        my_loader.dismiss()
        val intent = Intent(this@UploadActivity, OrderActivity::class.java)
        intent.putExtra("product_name", product_name);
        intent.putExtra("product_id", product_id);
        intent.putExtra("product_image", product_image);
        startActivity(intent)

        super.onBackPressed()
    }

    companion object {
        //GalleryAdapter adapter;
        private val CHOOSE_FILE = arrayOf("Camera", "File manager")
    }
    class UserListAdapter(private var activity: Activity, private var items: ArrayList<String>): BaseAdapter() {

        private class ViewHolder(row: View?) {
            var txtName: ImageView? = null
            var txtComment: TextView? = null

            init {
                this.txtName = row?.findViewById<ImageView>(R.id.IM_imagename)
                this.txtComment = row?.findViewById<TextView>(R.id.TV_imagename)
            }
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view: View?
            val viewHolder: ViewHolder
            if (convertView == null) {
                val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                view = inflater.inflate(R.layout.upload_list_item, null)
                viewHolder = ViewHolder(view)
                view?.tag = viewHolder
            } else {
                view = convertView
                viewHolder = view.tag as ViewHolder
            }


            viewHolder.txtComment?.text = items[position]


            return view as View
        }

        override fun getItem(i: Int): String {
            return items[i]
        }

        override fun getItemId(i: Int): Long {
            return i.toLong()
        }

        override fun getCount(): Int {
            return items.size
        }
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(true)
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
}
