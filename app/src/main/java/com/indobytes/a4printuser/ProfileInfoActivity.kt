package com.indobytes.a4printuser

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.Helper.ConnectionManager
import com.indobytes.a4printuser.model.APIResponse.UpdateProfileResponse
import com.indobytes.a4printuser.model.APIResponse.User_info
import com.indobytes.a4printuser.model.ApiInterface
import kotlinx.android.synthetic.main.app_bar_main.*
import retrofit2.Call
import retrofit2.Callback


class ProfileInfoActivity : AppCompatActivity() {
    internal lateinit var tv_full_name_id: TextView
    internal lateinit var tv_user_name_id:TextView
    internal lateinit var tv_email_id:TextView
    internal lateinit var tv_company_id:TextView
    internal lateinit var tv_phone_no_id:TextView
    internal lateinit var tv_alt_no_id:TextView
    internal lateinit var tv_cust_serv_id:TextView
    internal lateinit var tv_ref_by_id:TextView
    internal lateinit var user_id: String
    internal lateinit var email: String
    internal lateinit var firstname: String
    internal lateinit var lastname: String
    internal lateinit var company: String
    internal lateinit var phonenumber: String
    internal lateinit var alternatenumber: String
    internal lateinit var csr: String
    internal lateinit var refferrby: String
    internal lateinit var tv_edit_profile: TextView
    public var mProfileData: ArrayList<User_info> = ArrayList<User_info>()
    internal lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_info)
        sessionManager = SessionManager(this)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setTitle("PROFILE")
        var rootview = findViewById(R.id.rootview) as LinearLayout
        myloading()

        tv_full_name_id = findViewById(R.id.tv_full_name_id) as TextView
        tv_user_name_id = findViewById(R.id.tv_user_name_id) as TextView
        tv_email_id = findViewById(R.id.tv_email_id) as TextView
        tv_company_id = findViewById(R.id.tv_company_id) as TextView
        tv_phone_no_id = findViewById(R.id.tv_phone_no_id) as TextView
        tv_alt_no_id = findViewById(R.id.tv_alt_no_id) as TextView
        tv_cust_serv_id = findViewById(R.id.tv_cust_serv_id) as TextView
        tv_ref_by_id = findViewById(R.id.tv_ref_by_id) as TextView
        tv_edit_profile = findViewById(R.id.tv_edit_profile) as TextView
        if(ConnectionManager.checkConnection(applicationContext)) {
            getProfileInfo()
        }
        else{
            ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview,applicationContext)
        }
        tv_edit_profile.setOnClickListener {
            val intent = Intent(this@ProfileInfoActivity, ProfileEditActivity::class.java)
            intent.putExtra("username",user_id)
            intent.putExtra("email",email)
            intent.putExtra("firstname",firstname)
            intent.putExtra("lastname",lastname)
            intent.putExtra("company",company)
            intent.putExtra("phonenumber",phonenumber)
            intent.putExtra("alternatenumber",alternatenumber)
            intent.putExtra("csr",csr)
            intent.putExtra("refferrby",refferrby)
            finish()
            startActivity(intent)
        }
    }

    private fun getProfileInfo() {
        val apiService = ApiInterface.create()
        my_loader.show()
        val call = apiService.getProfile(ApiInterface.ENCRIPTION_KEY,sessionManager.isUserId)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UpdateProfileResponse> {
            override fun onResponse(call: Call<UpdateProfileResponse>, response: retrofit2.Response<UpdateProfileResponse>?) {
                my_loader.dismiss()
                if (response != null) {
                    Log.w("Result_Address_Profile","Result : "+response.body()!!.status)
                    if(response.body()!!.status.equals("1")) {
                            setProfileDataView(response.body()!!.user_info)
                            setData(response.body()!!.user_info)
                        
                            Log.w("Result_Address_Profile","Data : "+mProfileData)
                    }

                }
            }

            override fun onFailure(call: Call<UpdateProfileResponse>, t: Throwable) {
                Log.w("Result_Address_Profile",t.toString())
            }
        })
    }

    private fun setData(user_info: User_info?) {
        user_id = user_info!!.username!!
        email = user_info!!.email!!
        firstname = user_info!!.first_name!!
        lastname = user_info!!.last_name!!
        company = user_info!!.company!!
        phonenumber = user_info!!.phone_number!!
        alternatenumber = user_info!!.alternate_number!!
        if(user_info.customer_service_representative==null)
            user_info.customer_service_representative=""
        if(user_info.referred_by==null)
            user_info.referred_by=""
        csr = user_info!!.customer_service_representative!!
        refferrby = user_info!!.referred_by!!

    }

    private fun setProfileDataView(mProfileData: User_info?) {
        tv_edit_profile = findViewById(R.id.tv_edit_profile) as TextView
        tv_full_name_id.setText(mProfileData!!.first_name + " " + mProfileData.last_name)
        tv_user_name_id.setText(mProfileData!!.username)
        tv_email_id.setText(mProfileData.email)
        tv_company_id.setText(mProfileData.company)
        tv_phone_no_id.setText(mProfileData.phone_number)
        tv_alt_no_id.setText(mProfileData.alternate_number)
        tv_cust_serv_id.setText(mProfileData.customer_service_representative)
        tv_ref_by_id.setText(mProfileData.referred_by)

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }
}
