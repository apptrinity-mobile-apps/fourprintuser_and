package com.indobytes.a4printuser

import android.app.Dialog
import android.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.TextView
import com.indobytes.a4print.Session.SessionManager
import com.indobytes.a4printuser.adapter.PageAdapter
import com.indobytes.a4printuser.fragment.PageFragment
import com.indobytes.a4printuser.fragment.ProductFragment
import com.indobytes.a4printuser.model.ApiInterface
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.json.JSONArray
import org.json.JSONObject
import android.support.v4.view.ViewCompat.setAlpha
import android.view.ViewGroup




class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var fragmentName: Fragment? = null
    internal lateinit var sessionManager: SessionManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sessionManager = SessionManager(this)
        setSupportActionBar(toolbar)
        setTitle("DASHBOARD")
        toolbar.setTitleTextAppearance(this, R.style.RobotoBoldTextAppearance)
        setTabPages()
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        val header = nav_view.getHeaderView(0)
        val tv_nav_header_name_id = header.findViewById(R.id.username_txt_view) as TextView
        tv_nav_header_name_id.setText(sessionManager.isUserName)
    }

    private fun setTabPages() {
        val pageAdapter = PageAdapter(supportFragmentManager)

        // create fragments from 0 to 9
        var title: String = ""
        for (i in 0 until 4) {
            pageAdapter.add(PageFragment.newInstance(i), title)
        }

        view_pager.adapter = pageAdapter
        tabs.setupWithViewPager(view_pager)
        pageAdapter.notifyDataSetChanged()
        view_pager.addOnPageChangeListener(
                TabLayout.TabLayoutOnPageChangeListener(tabs))

        tabs.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                view_pager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })

    }

    private fun pushFragment(type:String, id: Int) {
        view_pager.visibility=View.GONE
        tabs.visibility=View.GONE
        if(type=="product"){
            fragmentName = ProductFragment()
        }
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(id, fragmentName,type)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    override fun onBackPressed() {
        for (fragment in supportFragmentManager.fragments) {
            supportFragmentManager.beginTransaction().remove(fragment).commit()
            view_pager.visibility=View.VISIBLE
            tabs.visibility=View.VISIBLE
            setTabPages()
        }
        setTitle("DASHBOARD")
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    //var item2: MenuItem? = null
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.

        when (item.itemId) {
            R.id.nav_profile -> {
                val intent = Intent(this,ProfileInfoActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_products -> {
                pushFragment("product",R.id.content_layout_products)
               // item2 = item
            }
            R.id.nav_address -> {
                val intent = Intent(this,MyAddressActivity::class.java)
                startActivity(intent)

            }
            R.id.nav_changepassword -> {
                val intent = Intent(this,ChangePasswordActivity::class.java)
                startActivity(intent)

            }
            R.id.nav_myorders-> {
               val intent = Intent(this,MainActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            R.id.nav_logout -> {
                sessionManager.logoutUser()

            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onPostResume() {
        removeFragment()
        super.onPostResume()
    }

    private fun removeFragment() {
        try {
            val fm = fragmentManager
            for (i in fm.backStackEntryCount - 1 downTo 1) {
                if (!fm.getBackStackEntryAt(i).name.equals("product")) {
                   // item2!!.setChecked(false)
                    fm.popBackStack()
                } else {
                    break
                }
            }
        }catch (e:Exception){
            e.printStackTrace()
        }

    }
    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

}
